﻿using Autofac;
using System.Reflection;
using UniverCert.Shared.Data.Repositories;
using UniverCert.Shared.Data.Repositories.ControllType.cs;
using UniverCert.Shared.Data.Repositories.EducationLevel;
using UniverCert.Shared.Data.Repositories.Stage;
using UniverCert.Shared.Data.Repositories.Student;

namespace OffChain.Api
{
    public class AutofacModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = Assembly.Load(new AssemblyName("OffChain.Api"));
            builder.RegisterAssemblyTypes(assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<UserRepo>().As<IUserRepo>().InstancePerLifetimeScope(); //добавление DI сервисы  
            builder.RegisterType<OrganizationRepo>().As<IOrganizationRepo>().InstancePerLifetimeScope();
            builder.RegisterType<SubjectRepo>().As<ISubjectRepo>().InstancePerLifetimeScope();
            builder.RegisterType<SpecialityRepo>().As<ISpecialityRepo>().InstancePerLifetimeScope();
            builder.RegisterType<StudentSpecialityLinkRepo>().As<IStudentSpecialityLinkRepo>().InstancePerLifetimeScope();
            builder.RegisterType<StudentRepo>().As<IStudentRepo>().InstancePerLifetimeScope();
            builder.RegisterType<StageRepo>().As<IStageRepo>().InstancePerLifetimeScope();
            builder.RegisterType<EducationLevelRepo>().As<IEducationLevelRepo>().InstancePerLifetimeScope();
            builder.RegisterType<ControllTypeRepo>().As<IControllTypeRepo>().InstancePerLifetimeScope(); 
        }
    }
}
