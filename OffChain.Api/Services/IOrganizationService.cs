﻿using OffChain.Api.Models;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Helpers;
using UniverCert.Shared.Data.Variables;

namespace OffChain.Api.Services
{
    public interface IOrganizationService<TEntity>
    {
        /// <summary>
        /// Метод возвращает информацию об организации 
        /// </summary>
        /// <param name="id">Ид организации</param>
        /// <returns>Информация об организации</returns>
        Task<object> GetOrganizationInfo(int id);

        /// <summary>
        /// Метод добавляет новую организацию в базу и возвращает Ид организации
        /// </summary>
        /// <param name="org">Данные организации</param>
        /// <returns>Ид организации</returns>
        Task<int> Add(Organization org);

        /// <summary>
        /// Метод обновляет  организацию в базе и возвращает Ид организации
        /// </summary>
        /// <param name="org">Данные организации</param>
        /// <returns>Сообщение о статусе операции</returns>
        Task<string> Update(Organization org);

        Task<IEnumerable<TEntity>> GetList(OrganizationRole _role);

        Task<PaginatedLists<Organization>> Search(OrganizationSearch query);
        /// <summary>
        /// Получение список типы организации
        /// </summary>
        /// <returns>Список[key,value]</returns>
        IEnumerable GetTypes();
    }
}
