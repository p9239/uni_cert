﻿using Microsoft.EntityFrameworkCore;
using OffChain.Api.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Helpers;
using UniverCert.Shared.Data.Repositories;
using UniverCert.Shared.Data.Variables;

namespace OffChain.Api.Services
{
    public class OrganizationService : IOrganizationService<Organization>
    {
        private readonly IOrganizationRepo _repo;

        public OrganizationService(IOrganizationRepo repo)
        {
            _repo = repo;
        }

        public async Task<object> GetOrganizationInfo(int id)
        {
            var _ = await _repo.GetQueryable(x => x.Id == id).FirstOrDefaultAsync();

            if (_ == null)
                return "Organization not found";
            return new {_.Id, _.BIN, _.Address, _.NameKZ, _.NameRU, _.NameEN,  _.CreatedDate, OrganizationType = Enum.GetName(typeof(OrganizationRole), _.Role), _.IsDeleted };
        }

        public async Task<int> Add(Organization org)
        {
            if (org != null)
            {
                var _ = await _repo.Add(org);
                return _;
            }
            return 0;
        }


        public async Task<string> Update(Organization org)
        {
            if (org != null)
            {
                await _repo.Update(org);
                return "updated";
            }
            return "Organization is null";
        }

        public async Task<IEnumerable<Organization>> GetList(OrganizationRole _role)
        {
            return await _repo.GetQueryable(x => (_role == OrganizationRole.NONE || x.Role == _role) && x.IsDeleted == false).ToListAsync();
        }

        public async Task<PaginatedLists<Organization>> Search(OrganizationSearch query)
        {
            var _ = _repo.Base()
                .Where(x =>
                (query.Id == default || x.Id == query.Id) &&
                (query.BIN == default || x.BIN.ToLower().StartsWith(query.BIN.ToLower())) &&
                (query.NameKZ == default || x.NameKZ.ToLower().Contains(query.NameKZ.ToLower())) &&
                (query.NameRU == default || x.NameRU.ToLower().Contains(query.NameRU.ToLower())) &&
                (query.NameEN == default || x.NameEN.ToLower().Contains(query.NameEN.ToLower())) &&
                (query.Address == default || x.Address.ToLower().Contains(query.Address.ToLower()))
                );
            return await PaginatedLists<Organization>.CreateAsync(_.AsNoTracking(), query.Page ?? 1, 20);

    }

        public IEnumerable GetTypes()
        {
           var _ = Enum.GetValues(typeof(OrganizationRole));
           Dictionary<int, string> dict = new Dictionary<int, string>();
            foreach(OrganizationRole v in _)
            {
                if ((v == OrganizationRole.NONE) || (v == OrganizationRole.Ministry))
                    continue;

                string desc = v.GetAttributeOfType<DescriptorAttribute>().Description;
                dict.Add((int)v, desc);
            }
            return dict.Select(x=> new
            {
                key = x.Key,
                value = x.Value
            }).ToArray();
        }
    }
}
