﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using UniverCert.Shared.Data.Repositories;

namespace OffChain.Api.Services
{
    public class UserInfoService : IUserInfoService
    {
        private readonly IUserRepo _repo;

        public UserInfoService(IUserRepo repo)
        {
            _repo = repo;
        }
        public async Task<object> GetInfo(string login)
        {
           var user = await _repo.GetQueryable(x => x.Login == login).FirstOrDefaultAsync();

            if (user == null)
                return "user not found";
            return new {user.Name,user.Id,user.CreatedDate};
        }
    }
}
