﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Repositories;
using UniverCert.Shared.Data.Variables;

namespace OffChain.Api.Services
{
    /// <summary>
    /// Сервис по работе с каталогом дисциплин OfChain базы
    /// </summary>
    public class SubjectService : ICatalogService<Subject, long>
    {
        private readonly ISubjectRepo _repo; 

        public SubjectService(ISubjectRepo repo)
        {
            _repo = repo;
        }

        /// <summary>
        /// Добавления дисциплины в каталог
        /// </summary>
        /// <param name="sub"></param>
        /// <returns></returns>
        public async Task<long> Add(Subject sub)
        {
            if (sub != null)
            {
                var _ = await _repo.Add(sub);
                return _;
            }
            return 0;
        }
         
         

        /// <summary>
        /// Метод обновляет данных списка дисциплин в OfChain базе  
        /// </summary>
        /// <param name="subjects">Список дисциплин</param>
        /// <returns>Информацию о выполненной операции </returns>
        public async Task<string> UpdateAll(List<Subject> subjects)
        {
            try
            {
                if (subjects != null && subjects.Count() > 0)
                {
                    await _repo.UpdateRange(subjects);
                    return "updated";
                }
                return "subjects list empty";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// Метод возвращает объект дисциплины из каталога OfChain базы  по ИД    
        /// </summary>
        /// <param name="id">ИД дисциплины из OfChain базы </param>
        /// <returns>Соответсвующую дисциплину из OfChain базы</returns>
        public async Task<object> GetInfo(long id)
        {
            var _ = await _repo.GetQueryable(x => x.Id == id).FirstOrDefaultAsync();

            if (_ == null)
                return "Subject not found";
            return new
            {
                _.Id,
                _.CodeEN,
                _.CodeKZ,
                _.CodeRU,
                _.NameKZ,
                _.NameRU,
                _.NameEN,
                _.LmsSubjectId,
                _.DescEN,
                _.DescKZ,
                _.DescRU,
                _.CreatedDate,
                _.IsDeleted,
                SubjectType = Enum.GetName(typeof(SubjectType), _.Type)
            };
        }

        public async Task<Subject> GetInfo(int orgId, long LmsItemId)
        {
            return await _repo.GetQueryable(x => x.OrganizationId == orgId && x.LmsSubjectId == LmsItemId).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Метод возвращает объект дисциплины из каталога OfChain базы  по ИД дисциплины из LMS системы Вуза
        /// </summary>
        /// <param name="lmsSubjectId">ИД дисциплины из LMS системы Вуза</param>
        /// <returns>Соответсвующую дисциплину из OfChain базы</returns>
        public async Task<object> GetInfoByLmsId(long lmsSubjectId)  
        {
            var _ = await _repo.GetQueryable(x => x.LmsSubjectId == lmsSubjectId).FirstOrDefaultAsync();

            if (_ == null)
                return "Subject not found";
            return new
            {
                _.Id,
                _.CodeEN,
                _.CodeKZ,
                _.CodeRU,
                _.NameKZ,
                _.NameRU,
                _.NameEN,
                _.LmsSubjectId,
                _.DescEN,
                _.DescKZ,
                _.DescRU,
                _.CreatedDate,
                SubjectType = Enum.GetName(typeof(SubjectType), _.Type)
            };
        }

        /// <summary>
        /// Метод возвращает список дисциплин по Ид Вуза 
        /// </summary>
        /// <param name="organizationId">Ид Вуза</param>
        /// <returns></returns>
        public async Task<IEnumerable<Subject>> GetList(long organizationId)
        {  
            return await _repo.GetQueryable(x => x.OrganizationId == organizationId).ToListAsync();
        }


        /// <summary>
        /// Метод возвращает список дисциплин по Ид Вуза и ИД дисциплин 
        /// </summary>
        /// <param name="orgId">Ид Вуза</param>
        /// <param name="subjectIds">ИД дисциплин из LMS системы Вуза</param>
        /// <returns></returns>
        public async Task<IEnumerable<Subject>> GetList(int orgId, IEnumerable<long> subjectIds)
        {
            return await _repo.GetQueryable(x => x.OrganizationId == orgId && subjectIds.Contains(x.Id)).ToListAsync();
        }

         

        public async Task AddRange(IEnumerable<Subject> subs)
        {
            if (subs != null && subs.Count() > 0)
            {
                await _repo.AddRange(subs);
            }
        }

        public async Task Update(Subject entity)
        {
            await _repo.Update(entity);
        }

        public async Task<IEnumerable<Subject>> GetList(int orgId)
        { 
            return await _repo.GetQueryable(x => x.OrganizationId == orgId).ToListAsync();
        }

        public async Task<IEnumerable<Subject>> GetListByLmsIds(int orgId, IEnumerable<int> lmsItemIds)
        {
            return await _repo.GetQueryable(x => x.OrganizationId == orgId && lmsItemIds.Contains(x.LmsSubjectId)).ToListAsync();
        }
    }
}
