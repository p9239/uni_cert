﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Repositories;
using UniverCert.Shared.Data.Variables;

namespace OffChain.Api.Services
{
    public class SpecialityService : ICatalogService<Speciality, long>
    {
        private readonly ISpecialityRepo _repo;
         

        public SpecialityService(ISpecialityRepo repo)
        {
            _repo = repo;
        }

        public async Task<long> Add(Speciality spec)
        {
            if (spec != null)
            {
                var _ = await _repo.Add(spec);
                return _;
            }
            return 0;
        }

        public async Task AddRange(IEnumerable<Speciality> entities)
        {
            if (entities != null && entities.Count() > 0)
            {
                await _repo.AddRange(entities);
            }
        }
         

        public async Task<object> GetInfo(long id)
        {
            var _ = await _repo.GetQueryable(x => x.Id == id).FirstOrDefaultAsync();

            if (_ == null)
                return "Speciality not found";
            return new
            {
                _.Id,
                _.Code,
                _.TotalCredit,
                _.NameKZ,
                _.NameRU,
                _.NameEN,
                _.LmsSpecId,
                _.OrganizationId,
                _.QualificationEN,
                _.QualificationKZ,
                _.QualificationRU,
                _.CreatedDate,
                SpecialityType = Enum.GetName(typeof(SpecialityType), _.Type)
            };
        }

        public async Task<Speciality> GetInfo(int orgId, long LmsItemId)
        {
            return await _repo.GetQueryable(x => x.OrganizationId == orgId && x.LmsSpecId == LmsItemId).Include(x=>x.Organization).FirstOrDefaultAsync(); 
        }

        public async Task<IEnumerable<Speciality>> GetList(int orgId, IEnumerable<long> items)
        {
            return await _repo.GetQueryable(x => x.OrganizationId == orgId && items.Contains(x.Id)).Include(x => x.Organization).ToListAsync();
        }

        public async Task<IEnumerable<Speciality>> GetList(int orgId)
        {
            return await _repo.GetQueryable(x => x.OrganizationId == orgId).ToListAsync();
        }

        public async Task<IEnumerable<Speciality>> GetListByLmsIds(int orgId, IEnumerable<int> lmsItemIds)
        {
            return await _repo.GetQueryable(x => x.OrganizationId == orgId && lmsItemIds.Contains(x.LmsSpecId)).Include(x => x.Organization).ToListAsync();
        }

        public async Task Update(Speciality entity)
        {
            await _repo.Update(entity);
        }
    }
}
