﻿using System.Threading.Tasks;

namespace OffChain.Api.Services
{
    public interface IUserInfoService
    {
        Task<object> GetInfo(string login);
    }
}
