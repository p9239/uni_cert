﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Repositories.ControllType.cs;

namespace OffChain.Api.Services
{
    public class ControllTypeService : IReadOnlyCatalogService<ControllType, int>
    {
        private readonly IControllTypeRepo _repo;

        public ControllTypeService(IControllTypeRepo repo)
        {
            _repo = repo;
        }

        public async Task<int> Add(ControllType entity)
        {
            if (entity != null)
            {
                var _ = await _repo.Add(entity);
                return _;
            }
            return 0;
        }

        public async Task Update(ControllType entity)
        {
            await _repo.Update(entity);
        }

        public async Task<ControllType> Get(int id)
        {
            return await _repo.GetById(id);
        }

        public async Task<object> GetInfo(int id)
        {
            var _ = await _repo.GetQueryable(x => x.Id == id).FirstOrDefaultAsync();
            if (_ == null)
                return "Stage not found";
            return _;
        }

        public async Task<IEnumerable<ControllType>> GetList()
        {
            return await _repo.Base().ToListAsync();
        }

        public async Task<IEnumerable<ControllType>> GetList(IEnumerable<int> ids)
        {
            return await _repo.GetQueryable(s => ids.Contains(s.Id)).ToListAsync();
        }
    }
}
