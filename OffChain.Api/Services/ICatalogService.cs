﻿using OffChain.Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Helpers;

namespace OffChain.Api.Services
{
    public interface ICatalogService<TEntity, T>
    {
        Task<object> GetInfo(T id);

        Task<TEntity> GetInfo(int orgId, T LmsItemId);  

        Task<T> Add(TEntity entity);

        Task Update(TEntity entity);

        Task AddRange(IEnumerable<TEntity> entities);

        Task<IEnumerable<TEntity>> GetList(int orgId, IEnumerable<long> items);
        Task<IEnumerable<TEntity>> GetListByLmsIds(int orgId, IEnumerable<int> lmsItemIds); 

        Task<IEnumerable<TEntity>> GetList(int orgId);
         
    }

    public interface IStudentSpecialityLinkService
    {
        Task DeleteRange(IEnumerable<StudentSpecialityLink> entities);
        Task AddRange(IEnumerable<StudentSpecialityLink> entities);
        Task UpdateRange(IEnumerable<StudentSpecialityLink> entities); 
    }

    public interface IStudentService 
    {
        Task<object> GetInfo(long id);

        Task<Student> GetById(long id);
          
        Task<long>  Add(Student entity);
        Task Update(Student entity);

        Task AddRange(IEnumerable<Student> entities);

        Task<IEnumerable<Student>> GetList(int orgId, IEnumerable<long> items);
        Task<IEnumerable<Student>> GetListByLmsIds(int orgId, IEnumerable<int> lmsItemIds);

        Task<IEnumerable<Student>> GetList(int orgId);

        Task<PaginatedLists<Student>> Search(StudentSearch query);

        Task<Student> SearchOne(StudentSearch query);

    }

    public interface IReadOnlyCatalogService<TEntity, T>
    {
        Task<object> GetInfo(T id);

        Task<TEntity> Get(T id);

        Task<IEnumerable<TEntity>> GetList();
        Task<IEnumerable<TEntity>> GetList(IEnumerable<T> ids);
    }
     
}
