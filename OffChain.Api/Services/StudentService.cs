﻿using Microsoft.EntityFrameworkCore;
using OffChain.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Helpers;
using UniverCert.Shared.Data.Repositories;

namespace OffChain.Api.Services
{
    public class StudentService : IStudentService 
    {
        private readonly IStudentRepo _repo;
        private readonly IStudentSpecialityLinkService _serviceSpecialityLink;

        public StudentService(IStudentRepo repo, IStudentSpecialityLinkService serviceSpecialityLink)
        {
            _repo = repo;
            _serviceSpecialityLink = serviceSpecialityLink; 
        }

        public async Task<long> Add(Student entity)
        {

            if (entity != null)
            {
                List<StudentSpecialityLink> specialityList = entity.Specialities.ToList();
                
                entity.Specialities = null;
                var _ = await _repo.Add(entity);
                
                if (specialityList != null && specialityList.Count() > 0)
                {
                    foreach (var item in specialityList)
                    {
                        item.StudentId = _; 
                    }
                    await _serviceSpecialityLink.AddRange(specialityList);
                }
                return _;
            }
            return 0;
        }

        public async Task AddRange(IEnumerable<Student> entities)
        {
            if (entities != null && entities.Count() > 0)
            {
                await _repo.AddRange(entities);
            }
        }

        public async Task<object> GetInfo(long id)
        {
            var _ = await _repo.Base().Include(l => l.Specialities).Where(x => x.Id == id).FirstOrDefaultAsync(); 

            if (_ == null)
                return "Speciality not found";
            return _;
        }

        public async Task<Student> GetById(long id)
        { 
            return await _repo.GetById(id);
        } 


        public async Task<object> GetByIIN(string iin)
        {
            if (string.IsNullOrEmpty(iin))
                return null;
            var _ = await _repo.Base().Include(l => l.Specialities).Where(x => x.IIN.Trim() == iin.Trim()).FirstOrDefaultAsync();

            if (_ == null)
                return "Speciality not found";
            return _;
        }

        public async Task<IEnumerable<Student>> GetList(int orgId, IEnumerable<long> items)
        { 
            return await _repo.Base().Include(x => x.Specialities).ThenInclude(y => y.Speciality).Where(x => x.Specialities.Select(y => y.Speciality.OrganizationId).Contains(orgId) && items.Contains(x.Id)).ToListAsync();
        }

        public async Task<IEnumerable<Student>> GetList(int orgId)
        { 
            return await _repo.Base().Include(l => l.Specialities).Where(x => x.Specialities.Select(y => y.Speciality.OrganizationId).Contains(orgId)).ToListAsync();  
        }

        public async Task<PaginatedLists<Student>> Search(StudentSearch query)
        {
            var _ = _repo.Base().Include(l => l.Specialities)
               .ThenInclude(y => y.Speciality)
               .ThenInclude(y => y.Organization)
               .Where(x =>
                   (query.StudentId == default || x.Id == query.StudentId) &&
                   (query.LmsStudentId == default || x.LmsStudentId == query.LmsStudentId) &&
                   (query.IIN == default || x.IIN.StartsWith(query.IIN)) &&
                   (query.Sname == default || x.Sname.ToLower().StartsWith(query.Sname.ToLower())) &&
                   (query.Name == default || x.Name.ToLower().StartsWith(query.Name.ToLower())) &&
                   (query.Fname == default || x.Fname.ToLower().StartsWith(query.Fname.ToLower())) &&
                   (query.OrganizationId == default || x.Specialities.Select(o => o.Speciality.OrganizationId).Contains(query.OrganizationId))
               );
            return  await PaginatedLists<Student>.CreateAsync(_.AsNoTracking(), query.Page ?? 1, 20);
        }

        public async Task<Student> SearchOne(StudentSearch query) 
        {
            return await _repo.Base().Include(l => l.Specialities).ThenInclude(y => y.Speciality)             
                .Where(x =>
                   (query.StudentId == default || x.Id == query.StudentId) &&
                   (query.LmsStudentId == default || x.LmsStudentId == query.LmsStudentId) &&
                   (query.IIN == default || x.IIN.StartsWith(query.IIN)) &&
                   (query.Sname == default || x.Sname.ToLower().StartsWith(query.Sname.ToLower())) &&
                   (query.Name == default || x.Name.ToLower().StartsWith(query.Name.ToLower())) &&
                   (query.Fname == default || x.Fname.ToLower().StartsWith(query.Fname.ToLower())) &&
                   (query.OrganizationId == default || x.Specialities.Select(o => o.Speciality.OrganizationId).Contains(query.OrganizationId))
               ).AsNoTracking()
               .FirstOrDefaultAsync();
        }

        public async Task Update(Student entity)
        { 
           await _repo.Update(entity);
        }

        public async Task<IEnumerable<Student>> GetListByLmsIds(int orgId, IEnumerable<int> lmsItemIds)
        {
            return await _repo.GetQueryable(x => lmsItemIds.Contains(x.LmsStudentId)).Include(x => x.Specialities).ThenInclude(y => y.Speciality)/*.Where(x=> x.Specialities.Select(y => y.Speciality.OrganizationId).Contains(orgId))*/.AsNoTracking().ToListAsync();
        }
    }


}
