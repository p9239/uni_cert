﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Repositories.Student;

namespace OffChain.Api.Services
{
    public class StudentSpecialityLinkService : IStudentSpecialityLinkService
    {
        private readonly IStudentSpecialityLinkRepo _repo; 

        public StudentSpecialityLinkService(IStudentSpecialityLinkRepo repo)
        {
            _repo = repo;
        }
        public async Task AddRange(IEnumerable<StudentSpecialityLink> entities)
        {
            await _repo.AddRange(entities);
        }

        public async Task DeleteRange(IEnumerable<StudentSpecialityLink> entities)
        {
            await _repo.DeleteRange(entities);
        }

        public async Task UpdateRange(IEnumerable<StudentSpecialityLink> entities)
        {
            await _repo.UpdateRange(entities);
        }
    }
}
