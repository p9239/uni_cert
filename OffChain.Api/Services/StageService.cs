﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Repositories.Stage;

namespace OffChain.Api.Services
{
    public class StageService : IReadOnlyCatalogService<Stage, int>
    {
        private readonly IStageRepo _repo;

        public StageService(IStageRepo repo) 
        {
            _repo = repo;
        }
        public async Task<int> Add(Stage entity)
        {
            if (entity != null)
            {
                var _ = await _repo.Add(entity);
                return _;
            }
            return 0;
        }

        public async Task AddRange(IEnumerable<Stage> entities)
        {
            if (entities != null && entities.Count() > 0)
            {
                await _repo.AddRange(entities);
            }
        }

        public async Task<Stage> Get(int id)
        {
            return await _repo.GetById(id);
        }

        public async Task<object> GetInfo(int id)
        {
            var _ = await _repo.GetQueryable(x => x.Id == id).FirstOrDefaultAsync();

            if (_ == null)
                return "Stage not found";
            return new
            {
                _.Id, 
                _.NameKZ,
                _.NameRU,
                _.NameEN, 
                _.CreatedDate,
                _.IsDeleted
            };
        }
         

        public async Task<IEnumerable<Stage>> GetList()
        {
            return await _repo.Base().ToListAsync();
        }

        public async Task<IEnumerable<Stage>> GetList(IEnumerable<int> ids)
        {
            return  await _repo.GetQueryable(s => ids.Contains(s.Id)).ToListAsync();
        }

        public async Task Update(Stage entity)
        {
            await _repo.Update(entity);
        } 
    }
}
