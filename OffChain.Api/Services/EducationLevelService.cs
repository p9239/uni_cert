﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Repositories.EducationLevel;

namespace OffChain.Api.Services
{
    public class EducationLevelService : IReadOnlyCatalogService<EducationLevel, int>
    {
        private readonly IEducationLevelRepo _repo;

        public EducationLevelService(IEducationLevelRepo repo)
        {
            _repo = repo;
        }
        public async Task<int> Add(EducationLevel entity)
        {
            if (entity != null)
            {
                var _ = await _repo.Add(entity);
                return _;
            }
            return 0;
        }

        public async Task AddRange(IEnumerable<EducationLevel> entities)
        {
            if (entities != null && entities.Count() > 0)
            {
                await _repo.AddRange(entities);
            }
        }

        public async Task<EducationLevel> Get(int id)
        {
            return await _repo.GetById(id);
        }

        public async Task<object> GetInfo(int id)
        {
            var _ = await _repo.GetQueryable(x => x.Id == id).FirstOrDefaultAsync();

            if (_ == null)
                return "EducationLevel not found";
            return new
            {
                _.Id,
                _.NameKZ,
                _.NameRU,
                _.NameEN,
                _.CreatedDate,
                _.IsDeleted
            };
        }


        public Task<IEnumerable<EducationLevel>> GetList()
        {
            return (Task<IEnumerable<EducationLevel>>)_repo.GetAll();
        }

        public async Task<IEnumerable<EducationLevel>> GetList(IEnumerable<int> ids)
        {
            return ids.Count() > 0 ? await _repo.GetQueryable(e => ids.Contains(e.Id)).ToListAsync() : new List<EducationLevel>();
        }

        public async Task Update(EducationLevel entity)
        {
            await _repo.Update(entity);
        }
    }
}
