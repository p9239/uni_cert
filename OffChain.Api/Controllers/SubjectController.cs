﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OffChain.Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniverCert.Shared;
using UniverCert.Shared.Controllers;
using UniverCert.Shared.Data.Context;

namespace OffChain.Api.Controllers
{
    /// <summary>
    /// Каталог дисциплин
    /// </summary>
    public class SubjectController : BaseController
    {
        private readonly ICatalogService<Subject, long> _serviceSubject;

        public SubjectController(ICatalogService<Subject, long> serviceSubject)
        {
            _serviceSubject = serviceSubject;
        }


        /// <summary>
        /// Информаци о дисциплине
        /// </summary>
        /// <param name="id">Ид дисциплины </param>
        /// <returns>Информация о дисциплине</returns>
        [UserRole(RoleEnum.Owner)]
        [HttpGet("get")]
        public async Task<IActionResult> OrganizationInfo(int id)
        {
            try
            {
                if (id == default) { return BadRequest("Incorrect org id"); }
                object _ = await _serviceSubject.GetInfo(id);
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }


        /// <summary>
        /// Получить весь список дисциплин
        /// </summary> 
        /// <returns>Информация о дисциплин</returns>
        [UserRole(RoleEnum.Owner)]
        [HttpGet("all")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                if (CurrentUserOrganizationId == 0) { return BadRequest("Incorrect organization id"); }
                object _ = await _serviceSubject.GetList(CurrentUserOrganizationId);
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }

        /// <summary>
        /// Добавление дисциплины в базу системы
        /// </summary>
        /// <param name="subject">Данные дисциплины</param>
        /// <returns>ИД дисциплины </returns>
        [UserRole(RoleEnum.Owner)]
        [HttpPost("put")]
        public async Task<IActionResult> AddSubject([FromBody] Subject subject)
        {
            try
            {
                if (!ModelState.IsValid || subject.OrganizationId != CurrentUserOrganizationId) 
                    return BadRequest(ModelState);
                Subject dbRecord = await _serviceSubject.GetInfo(CurrentUserOrganizationId, subject.LmsSubjectId);
                 
                if (dbRecord != null)
                {
                    dbRecord.IsDeleted = subject.IsDeleted;
                    dbRecord.NameKZ = subject.NameKZ;
                    dbRecord.NameEN = subject.NameEN;
                    dbRecord.NameRU = subject.NameRU;
                    dbRecord.Type = subject.Type;
                    dbRecord.CodeEN = subject.CodeEN;
                    dbRecord.CodeKZ = subject.CodeKZ;
                    dbRecord.CodeRU = subject.CodeRU;
                    dbRecord.DescEN = subject.DescEN;
                    dbRecord.DescKZ = subject.DescKZ;
                    dbRecord.DescRU = subject.DescRU;
                    await _serviceSubject.Update(dbRecord);
                    return Ok(dbRecord.Id);
                }
                else
                {
                    var _ = await _serviceSubject.Add(subject);
                    return Ok(_);
                }
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }

        /// <summary>
        /// Добавление списка дисциплин в базу системы
        /// </summary>
        /// <param name="subjects">Данные дисциплин</param>
        /// <returns>ИД дисциплин в виде словаря &lt;key, value &gt; = &lt; lmsSubjectId, OfChainSubjectId &gt; </returns>
        [UserRole(RoleEnum.Owner)]
        [HttpPost("list/put")]
        public async Task<IActionResult> AddSubjects([FromBody] List<Subject> subjects)
        {
            int sid = 0;
            try
            {
                Dictionary<int, long> dic = new Dictionary<int, long>();
                if (!ModelState.IsValid) 
                    return BadRequest(ModelState);
                IEnumerable<Subject> dbList = await _serviceSubject.GetListByLmsIds(CurrentUserOrganizationId, subjects.Select(x=> x.LmsSubjectId));
                
                foreach (var subject in subjects)
                {
                    long id = 0; 
                    var dbRecord = dbList.Count() > 0 ? dbList.FirstOrDefault(x => x.LmsSubjectId == subject.LmsSubjectId) : null;
                    if (dbRecord != null)
                    {
                        id = dbRecord.Id;                         
                        dbRecord.IsDeleted = subject.IsDeleted;
                        dbRecord.NameKZ = subject.NameKZ;
                        dbRecord.NameEN = subject.NameEN;
                        dbRecord.NameRU = subject.NameRU;
                        dbRecord.Type = subject.Type;
                        dbRecord.CodeEN = subject.CodeEN;
                        dbRecord.CodeKZ = subject.CodeKZ;
                        dbRecord.CodeRU = subject.CodeRU;
                        dbRecord.DescEN = subject.DescEN;
                        dbRecord.DescKZ = subject.DescKZ;
                        dbRecord.DescRU = subject.DescRU;
                        await _serviceSubject.Update(dbRecord); 
                    }
                    else
                    {
                        subject.OrganizationId = CurrentUserOrganizationId;
                        subject.CreatedDate = DateTime.Now;
                        id =  await _serviceSubject.Add(subject);
                        subject.Id = id;
                    }
                    sid = subject.LmsSubjectId;
                    if(id > 0 && !dic.ContainsKey(subject.LmsSubjectId))
                        dic.Add(subject.LmsSubjectId, id);
                    else 
                        dic[subject.LmsSubjectId] = id;
                }
                string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
                return Ok(json);
            }
            catch (Exception e)
            {

                return ExceptionResult(e);
            }
        }
    }
}
