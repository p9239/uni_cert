﻿using Microsoft.AspNetCore.Mvc;
using OffChain.Api.Services;
using System;
using System.Threading.Tasks;
using UniverCert.Shared;
using UniverCert.Shared.Controllers;
using UniverCert.Shared.Data.Context;

namespace OffChain.Api.Controllers
{
    /// <summary>
    /// Справочник типы контроля
    /// </summary>
    public class ControllTypeController : BaseController
    {
        private readonly IReadOnlyCatalogService<ControllType, int> _service;

        public ControllTypeController(IReadOnlyCatalogService<ControllType, int> service)
        {
            _service = service;
        }

        /// <summary>
        /// Информаци о типе контроля
        /// </summary>
        /// <param name="id">Ид типа контроля </param>
        /// <returns>Информация о типе контроля</returns>
        [UserRole(RoleEnum.Owner)]
        [HttpGet("get")]
        public async Task<IActionResult> GetInfo(int id) 
        {
            try
            {
                if (id == default /*|| CurrentUserRole == RoleEnum.None*/) { return BadRequest("Incorrect controll type id"); }
                object _ = await _service.GetInfo(id);
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }


        /// <summary>
        /// Получить весь список типов контроля
        /// </summary> 
        /// <returns>Информация о типах контроля</returns>
        [UserRole(RoleEnum.Owner)]
        [HttpGet("all")]
        public async Task<IActionResult> GetAll()
        {
            try
            { 
                object _ = await _service.GetList();
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }
    }
}
