﻿using Microsoft.AspNetCore.Mvc;
using OffChain.Api.Services;
using System.Threading.Tasks;
using UniverCert.Shared;
using UniverCert.Shared.Controllers;
using UniverCert.Shared.Data.Context;

namespace OffChain.Api.Controllers
{
    public class TestController : BaseController
    {
        private readonly IUserInfoService _service;
        private readonly IOrganizationService<Organization> _serviceOrganization;
        public TestController(IUserInfoService service, IOrganizationService<Organization> serviceOrganization)
        {
            _service = service;
            _serviceOrganization = serviceOrganization;
        }

        /// <summary>
        /// Простой запрос
        /// </summary>
        /// <returns></returns>
        [HttpGet("Testing")]
        public string Get()
        {
            return "Проект OffChain.Api работает"; ;
        }

        /// <summary>
        /// Тест авторизация role:Owner
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [UserRole(RoleEnum.Owner)]
        [HttpGet("getUserInfo")]
        public async Task<IActionResult> UserInfo(string login)
        {
            if (string.IsNullOrEmpty(login)) { return BadRequest("Enter Email"); }
            var _ = await _service.GetInfo(login);
            return Ok(_);
        }
       
    }
}
