﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OffChain.Api.Models;
using OffChain.Api.Services;
using System;
using System.Threading.Tasks;
using UniverCert.Shared;
using UniverCert.Shared.Controllers;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Variables;

namespace OffChain.Api.Controllers
{
    public class OrganizationController : BaseController
    {
        private readonly IOrganizationService<Organization> _serviceOrganization;

        public OrganizationController(IOrganizationService<Organization> serviceOrganization)
        { 
            _serviceOrganization = serviceOrganization;
        }

        /// <summary>
        /// Информаци об организации
        /// </summary>
        /// <param name="id">Ид организации </param>
        /// <returns></returns>
        [UserRole(new RoleEnum[] { RoleEnum.Owner, RoleEnum.Head, RoleEnum.Verifier })]
        [HttpGet("get")]
        public async Task<IActionResult> OrganizationInfo(int id)
        {
            try
            {
                if (id == default) { return BadRequest("Enter org id"); }
                var _ = await _serviceOrganization.GetOrganizationInfo(id);
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }


        /// <summary>
        /// Поиск организации по запросу
        /// </summary>
        /// <param name="queryString">Строка запроса для поиска </param>
        /// <returns>Информация об организации</returns> 
        [HttpGet("search")]
        public async Task<IActionResult> OrganizationSearch([FromQuery] OrganizationSearch queryString) 
        {
            try
            {
                if (queryString == default) { return BadRequest("Incorrect query  parameters"); }
                object _ = await _serviceOrganization.Search(queryString);
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }


        /// <summary>
        /// Получить весь список организации
        /// </summary> 
        /// <returns>Информация об организациях</returns>
        [UserRole(new[] { RoleEnum.Head, RoleEnum.Owner })]
        [HttpGet("all")]
        public async Task<IActionResult> GetAll(int role = 0) 
        {
            try
            {
                if (!Enum.IsDefined(typeof(OrganizationRole), role))
                {
                    return BadRequest("Incorrect organization role");
                }
                object _ = await _serviceOrganization.GetList((OrganizationRole)role);
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }

        /// <summary>
        /// Добавление организации в базу системы
        /// </summary>
        /// <param name="org">Данные организации</param>
        /// <returns>ИД организации </returns>
        [UserRole(RoleEnum.Owner)]
        [HttpPost("put")]
        public async Task<IActionResult> AddOrganization([FromBody] Organization org)
        {
            try { 
                if (!ModelState.IsValid || CurrentUserRole == RoleEnum.None) return BadRequest(ModelState);
                if (org.Id > 0)
                {
                    var dbOrg = await _serviceOrganization.GetOrganizationInfo(org.Id);
                    if(dbOrg != null)
                    {
                        await _serviceOrganization.Update(org);
                    }
                }
                else
                {
                    org.Id = await _serviceOrganization.Add(org);
                }
                return Ok(org.Id);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }

        /// <summary>
        /// Получение список типы организации
        /// </summary>
        /// <returns>Информация об организации</returns> 
        [AllowAnonymous]
        [HttpGet("types")]
        public IActionResult GetTypes()
        {
            try
            {
                var _ = _serviceOrganization.GetTypes();
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }
    }
}
