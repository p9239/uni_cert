﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OffChain.Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks; 
using UniverCert.Shared;
using UniverCert.Shared.Controllers;
using UniverCert.Shared.Data.Context;

namespace OffChain.Api.Controllers
{
    public class SpecialityController : BaseController
    {
        private readonly ICatalogService<Speciality, long> _service;
        private readonly IReadOnlyCatalogService<Stage, int> _stageService;
        public SpecialityController(ICatalogService<Speciality, long> service, IReadOnlyCatalogService<Stage, int> stageService)
        {
            _service = service;
            _stageService = stageService;
        }


        /// <summary>
        /// Информаци о специальности
        /// </summary>
        /// <param name="id">Ид специальности </param>
        /// <returns>Информация о специальности</returns>
        [UserRole(RoleEnum.Owner)]
        [HttpGet("get")]
        public async Task<IActionResult> SpecialityInfo(int id) 
        {
            try
            {
                if (id == default /*|| CurrentUserRole == RoleEnum.None*/) { return BadRequest("Incorrect specliaty id"); }
                object _ = await _service.GetInfo(id);
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }


        /// <summary>
        /// Получить весь список специальности
        /// </summary> 
        /// <returns>Информация о специальности</returns>
        [UserRole(RoleEnum.Owner)]
        [HttpGet("all")]
        public async Task<IActionResult> GetAll() 
        {
            try
            {
                if (/*CurrentUserRole == RoleEnum.None ||*/ CurrentUserOrganizationId == 0) { return BadRequest("Incorrect organization id"); }
                object _ = await _service.GetList(CurrentUserOrganizationId);
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }

        /// <summary>
        /// Добавление специальности в базу системы
        /// </summary>
        /// <param name="speciality">Данные специальности</param>
        /// <returns>ИД специальности </returns>
        [UserRole(RoleEnum.Owner)]
        [HttpPost("put")]
        public async Task<IActionResult> AddSpeciality([FromBody] Speciality speciality)  
        {
            try
            {
                speciality.OrganizationId = CurrentUserOrganizationId;
                if (!ModelState.IsValid || speciality.OrganizationId != CurrentUserOrganizationId || speciality.LmsSpecId == default)
                    return BadRequest(ModelState);
                var stage = await _stageService.Get(speciality.StageId);
                if(stage == null)
                {
                    throw new ArgumentException("Ступень обучения не найдено в OfChain базе");
                }

                Speciality dbRecord = await _service.GetInfo(CurrentUserOrganizationId, speciality.LmsSpecId);
                long s = 0;
                if (dbRecord != null)
                {
                    s = dbRecord.Id;
                    dbRecord.Code = speciality.Code;
                    dbRecord.NameKZ = speciality.NameKZ;
                    dbRecord.NameRU = speciality.NameRU;
                    dbRecord.NameEN = speciality.NameEN;
                    dbRecord.QualificationEN = speciality.QualificationEN;
                    dbRecord.QualificationKZ = speciality.QualificationKZ;
                    dbRecord.QualificationRU = speciality.QualificationRU;
                    dbRecord.Type = speciality.Type;
                    dbRecord.TotalCredit = speciality.TotalCredit;
                    dbRecord.IsDeleted = speciality.IsDeleted;
                    dbRecord.StageId = speciality.StageId;
                    dbRecord.Stage = stage;
                    await _service.Update(dbRecord);
                }
                else
                {
                    speciality.CreatedDate = DateTime.Now;
                    s = await _service.Add(speciality);
                    speciality.Id = s;
                } 
                return Ok(s);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }

        /// <summary>
        /// Добавление списка специальности в базу системы
        /// </summary>
        /// <param name="specialities">Данные специальности</param>
        /// <returns>ИД специальности в виде словаря &lt;key, value &gt; = &lt; lmsSpecialityId, OfChainSpecialityId &gt; </returns>
        [UserRole(RoleEnum.Owner)]
        [HttpPost("list/put")]
        public async Task<IActionResult> AddSpecialities([FromBody] List<Speciality> specialities)   
        {
            try
            {
                Dictionary<int, long> dic = new Dictionary<int, long>();
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var stages = await _stageService.GetList(specialities.Select(x=>x.StageId));
                if (stages.Count() != specialities.Select(x => x.StageId).Distinct().Count())
                {
                    throw new ArgumentException("Некоторые ступени обучения не найдены в OfChain базе");
                }
                IEnumerable<Speciality> dbList = await _service.GetListByLmsIds(CurrentUserOrganizationId, specialities.Select(x =>  x.LmsSpecId));
                foreach (var item in specialities)
                {
                    long s = 0;
                    var dbRecord = dbList?.FirstOrDefault(x => x.LmsSpecId == item.LmsSpecId);
                    item.OrganizationId = CurrentUserOrganizationId;
                    if (dbRecord != null)
                    {
                        dbRecord.Code = item.Code;
                        dbRecord.NameKZ = item.NameKZ;
                        dbRecord.NameRU = item.NameRU;
                        dbRecord.NameEN = item.NameEN;
                        dbRecord.QualificationEN = item.QualificationEN;
                        dbRecord.QualificationKZ = item.QualificationKZ;
                        dbRecord.QualificationRU = item.QualificationRU;
                        dbRecord.Type = item.Type;
                        dbRecord.TotalCredit = item.TotalCredit;
                        dbRecord.IsDeleted = item.IsDeleted;
                        dbRecord.StageId = item.StageId;
                        await _service.Update(dbRecord);
                    }
                    else
                    {
                        item.CreatedDate = DateTime.Now;
                        s = await _service.Add(item);
                        item.Id = s;
                    }

                    if (s > 0 && !dic.ContainsKey(item.LmsSpecId))
                        dic.Add(item.LmsSpecId, s);
                }
                string json = dic != null ? JsonConvert.SerializeObject(dic, Formatting.Indented) : null;
                return Ok(json);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }
    }
}
