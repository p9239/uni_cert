﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OffChain.Api.Models;
using OffChain.Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniverCert.Shared;
using UniverCert.Shared.Controllers;
using UniverCert.Shared.Data.Context;

namespace OffChain.Api.Controllers
{
    /// <summary>
    /// Студенты Вуза
    /// </summary>
    public class StudentController : BaseController
    {

        private readonly IStudentService  _service;
        private readonly ICatalogService<Speciality, long> _specialityService;
        private readonly IStudentSpecialityLinkService _specialityLinkService;

        public StudentController(IStudentService  service, ICatalogService<Speciality, long> specialityService, IStudentSpecialityLinkService  specialityLinkService)
        {
            _service = service;
            _specialityService = specialityService;
            _specialityLinkService = specialityLinkService;
        }



        /// <summary>
        /// Информаци о студенте
        /// </summary>
        /// <param name="id">Ид студента </param>
        /// <returns>Информация о студенте</returns>
        [UserRole(RoleEnum.Owner)]
        [HttpGet("get")]
        public async Task<IActionResult> StudentInfo(long id) 
        {
            try
            {
                if (id == default) { return BadRequest("Incorrect org id"); }
                object _ = await _service.GetInfo(id);
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }


        /// <summary>
        /// Поиск студентов по запросу
        /// </summary>
        /// <param name="queryString">Строка запроса для поиска </param>
        /// <returns>Информация о студентах</returns> 
        [HttpGet("search")]
        public async Task<IActionResult> StudentSearch([FromQuery] StudentSearch queryString)
        {
            try
            {
                if (queryString == default) { return BadRequest("Incorrect query  parameters"); }
                object _ = await _service.Search(queryString);
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }

        /// <summary>
        /// Получить весь список студентов
        /// </summary> 
        /// <returns>Информация о студенте</returns>
        [UserRole(RoleEnum.Owner)]
        [HttpGet("all")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                if (CurrentUserOrganizationId == 0) { return BadRequest("Incorrect organization id"); }
                object _ = await _service.GetList(CurrentUserOrganizationId);
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }

        /// <summary>
        /// Добавление студента в базу системы
        /// </summary>
        /// <param name="student">Данные студента</param>
        /// <returns>ИД студента </returns>
        [UserRole(RoleEnum.Owner)]
        [HttpPost("put")]
        public async Task<IActionResult> AddStudent([FromBody] StudentDto student) 
        {
            try
            {  
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                IEnumerable<Speciality> spList = await _specialityService.GetListByLmsIds(CurrentUserOrganizationId, student.Specialities.Select(s => s.LmsSpecialityId)); // находим все специальности из базы
                if (spList.Count() != student.Specialities.Select(s => s.LmsSpecialityId).Distinct().Count()) // если не все специальности найдены из базы по данной организации, выводим ошибку
                {
                    throw new ArgumentException("Специальности с идентификаторами {" +
                        (student.Specialities != null && student.Specialities.Count() > 0 ? string.Join(",", student.Specialities.Select(x => x.LmsSpecialityId).Except(spList.Count() > 0 ? spList.Select(s => s.LmsSpecId) : new List<int>() { 0 })) : "") + "} не найдены в OfChain базе!");
                }

                Student dbStudent = await _service.SearchOne(query: new  StudentSearch() { IIN = student.IIN, OrganizationId = CurrentUserOrganizationId, LmsStudentId = student.LmsStudentId });
               
                if (dbStudent == null)
                {
                    var ofChainSpList = student.Specialities.Select(x =>
                           new StudentSpecialityLink()
                           {
                               StudentId = 0,
                               SpecialityId = spList.FirstOrDefault(y => y.LmsSpecId == x.LmsSpecialityId).Id
                           }
                        ).ToList();
                    Student bchainStudent = new Student();
                    bchainStudent.LmsStudentId = student.LmsStudentId;
                    bchainStudent.Name = student.Name;
                    bchainStudent.Sname = student.Sname;
                    bchainStudent.Fname = student.Fname;
                    bchainStudent.BirthDay = student.BirthDay;
                    bchainStudent.IIN = student.IIN;
                    bchainStudent.Specialities = ofChainSpList;
                    var _ = await _service.Add(bchainStudent);
                    return Ok(_);
                }
                else
                {
                    var ofChainSpList = student.Specialities.Select(x =>
                           new StudentSpecialityLink()
                           {
                               StudentId = dbStudent.Id,
                               SpecialityId = spList.FirstOrDefault(y => y.LmsSpecId == x.LmsSpecialityId).Id,
                               CreatedDate = DateTime.Now,
                               IsDeleted = false
                           }
                        ).ToList();
                    var updStudent = await _service.GetById(dbStudent.Id);
                    //Записываем измененные данные студента
                    updStudent.Sname = student.Sname;
                    updStudent.Name = student.Name;
                    updStudent.Fname = student.Fname;
                    updStudent.BirthDay = student.BirthDay;
                    updStudent.IsDeleted = student.IsDeleted;
                    updStudent.IIN = student.IIN;

                    await _service.Update(updStudent); //обновляем данные студента в базе

                    //Находим имеющиеся записи в базе для данной организации по данному студенту из линковой таблицы
                    var updatedSpec =
                    (from dbSpecLink in dbStudent.Specialities
                     join updStudSpecLink in ofChainSpList on
                             new { dbSpecLink.SpecialityId, dbSpecLink.StudentId }
                         equals
                             new { updStudSpecLink.SpecialityId, updStudSpecLink.StudentId }
                     
                     select new StudentSpecialityLink
                     {
                         Id = dbSpecLink.Id,
                         SpecialityId = dbSpecLink.SpecialityId,
                         StudentId = dbSpecLink.StudentId,
                         CreatedDate = dbSpecLink.CreatedDate,
                         IsDeleted = dbSpecLink.IsDeleted
                     }).ToList();

                    if (dbStudent.Specialities.Count() > 0 && updatedSpec.Count() > 0)
                    {
                        var oldDbSpec = dbStudent.Specialities.Where(s =>
                        !updatedSpec.Select(x => x.SpecialityId).Contains(s.SpecialityId) &&
                        !updatedSpec.Select(x => x.StudentId).Contains(s.StudentId)
                        ).ToList(); //из линковой таблицы находим все удаляемые записи по данному студенту по данной организации  

                        if (oldDbSpec.Count() > 0) // если в списке есть записи для удаления, то их удаляем
                        {
                            await _specialityLinkService.DeleteRange(oldDbSpec);
                        }
                    }

                    if (student.Specialities != null && student.Specialities.Count() > 0)
                    {
                        var newLink = dbStudent.Specialities.Count() > 0 ? ofChainSpList.Where(sn => !dbStudent.Specialities.Select(db => db.SpecialityId).Contains(sn.SpecialityId)).ToList() : ofChainSpList.ToList(); // для записи в базу добавляем новые записи
                        if (newLink.Count() > 0) // если в списке есть записи для добавления, то их добавялем
                        {
                            await _specialityLinkService.AddRange(newLink);
                        }
                    }
                    return Ok(dbStudent.Id);
                }
            } 
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }

        /// <summary>
        /// Добавление списка студента в базу системы
        /// </summary>
        /// <param name="students">Данные студента</param>
        /// <returns>ИД студента в виде словаря &lt;key, value &gt; = &lt; lmsSubjectId, OfChainSubjectId &gt; </returns>
        [UserRole(RoleEnum.Owner)]
        [HttpPost("list/put")]
        public async Task<IActionResult> AddStudents([FromBody] List<StudentDto> students)
        {
            try
            {
                Dictionary<int, long> dic = new Dictionary<int, long>();
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                var lmsSpecList = students.SelectMany(x => x.Specialities.Select(s => s.LmsSpecialityId)).Distinct();
                IEnumerable<Speciality> spList = await _specialityService.GetListByLmsIds(CurrentUserOrganizationId, lmsSpecList); // находим все специальности из базы
                if (spList.Count() != lmsSpecList.Count()) // если не все специальности найдены из базы по данной организации, выводим ошибку
                {
                    throw new ArgumentException("Специальности с идентификаторами {" +
                        (lmsSpecList.Count() > 0 ? string.Join(",", lmsSpecList.Except(spList.Count() > 0 ? spList.Select(s => s.LmsSpecId) : new List<int>() { 0 })) : "") + "} не найдены в OfChain базе!");
                }

                IEnumerable<Student> dbList = await _service.GetListByLmsIds(CurrentUserOrganizationId, students.Select(x => x.LmsStudentId));
               
                foreach (var student in students)
                {
                    long s = 0;
                    var dbStudent = dbList.FirstOrDefault(x => x.LmsStudentId == student.LmsStudentId);
                    if (dbStudent != null)
                    {
                        var ofChainSpList = student.Specialities.Select(x =>
                              new StudentSpecialityLink()
                              {
                                  StudentId = dbStudent.Id,
                                  SpecialityId = spList.FirstOrDefault(y => y.LmsSpecId == x.LmsSpecialityId).Id,
                                  CreatedDate = DateTime.Now,
                                  IsDeleted = false
                              }
                           ).ToList();
                        s = dbStudent.Id; 
                        var updStudent = await _service.GetById(dbStudent.Id);
                        //Записываем измененные данные студента
                        updStudent.Sname = student.Sname;
                        updStudent.Name = student.Name;
                        updStudent.Fname = student.Fname;
                        updStudent.BirthDay = student.BirthDay;
                        updStudent.IsDeleted = student.IsDeleted;
                        updStudent.IIN = student.IIN;
                        await _service.Update(updStudent);

                        //Находим имеющиеся записи в базе для данной организации по данному студенту из линковой таблицы
                        var updatedSpec =
                        (from dbSpecLink in dbStudent.Specialities
                         join updStudSpecLink in ofChainSpList on
                                 new { dbSpecLink.SpecialityId, dbSpecLink.StudentId }
                             equals
                                 new { updStudSpecLink.SpecialityId, updStudSpecLink.StudentId }

                         select new StudentSpecialityLink
                         {
                             Id = dbSpecLink.Id,
                             SpecialityId = dbSpecLink.SpecialityId,
                             StudentId = dbSpecLink.StudentId,
                             CreatedDate = dbSpecLink.CreatedDate,
                             IsDeleted = dbSpecLink.IsDeleted
                         }).ToList();

                        if (dbStudent.Specialities.Count() > 0 && updatedSpec.Count() > 0)
                        {
                            var oldDbSpec = dbStudent.Specialities.Where(s =>
                            !updatedSpec.Select(x => x.SpecialityId).Contains(s.SpecialityId) &&
                            !updatedSpec.Select(x => x.StudentId).Contains(s.StudentId)
                            ).ToList(); //из линковой таблицы находим все удаляемые записи по данному студенту по данной организации  

                            if (oldDbSpec.Count() > 0) // если в списке есть записи для удаления, то их удаляем
                            {
                                await _specialityLinkService.DeleteRange(oldDbSpec);
                            }
                        }

                        if (student.Specialities != null && student.Specialities.Count() > 0)
                        {
                            var newLink = dbStudent.Specialities.Count() > 0 ? ofChainSpList.Where(sn => !dbStudent.Specialities.Select(db => db.SpecialityId).Contains(sn.SpecialityId)).ToList() : ofChainSpList.ToList(); // для записи в базу добавляем новые записи
                            if (newLink.Count() > 0) // если в списке есть записи для добавления, то их добавялем
                            {
                                await _specialityLinkService.AddRange(newLink);
                            }
                        }
                    }
                    else
                    {
                        var ofChainSpList = student.Specialities.Select(x =>
                               new StudentSpecialityLink()
                               {
                                   StudentId = 0,
                                   SpecialityId = spList.FirstOrDefault(y => y.LmsSpecId == x.LmsSpecialityId).Id,
                                   CreatedDate = DateTime.Now,
                                   IsDeleted = false
                               }
                            ).ToList();
                        Student bchainStudent = new Student
                        {
                            LmsStudentId = student.LmsStudentId,
                            Name = student.Name,
                            Sname = student.Sname,
                            Fname = student.Fname,
                            BirthDay = student.BirthDay,
                            IIN = student.IIN,
                            Specialities = ofChainSpList,
                            CreatedDate = DateTime.Now
                        };
                        var _ = await _service.Add(bchainStudent);
                        s = _;
                    }

                    if (s > 0 && !dic.ContainsKey(student.LmsStudentId))
                        dic.Add(student.LmsStudentId, s);
                }
                string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
                return Ok(json);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }
    }
}
