﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OffChain.Api.Models;
using System;
using System.Net;
using System.Text;
using UniverCert.Shared.Data.Helpers;

namespace OffChain.Api.Controllers
{
    /// <summary>
    /// Тестовый APi приемник ответа о результате запроса от блокчейн системы
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class LmsApiController : Controller
    {

        [HttpPost]
        public JsonResult BlockChainQueueResult()
        {

            string token = null;
            var authorizationHeader = Request.Headers["Authorization"];
            var authorizationParts = authorizationHeader.ToString().Split(' ');
            if (authorizationParts.Length == 2 && authorizationParts[0] == "Bearer")
            {
                token = authorizationParts[1];
            }

            QueueResultModel json;
            try
            {
                json = SharedHelper.DecodeJWTToken<QueueResultModel>(token, "hereIsToken");
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(new { StatusCode = (int)HttpStatusCode.InternalServerError, mess = ex.Message });
            }


            return Json(json);
        }
    }
}
