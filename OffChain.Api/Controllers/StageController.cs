﻿using Microsoft.AspNetCore.Mvc;
using OffChain.Api.Services;
using System;
using System.Threading.Tasks;
using UniverCert.Shared;
using UniverCert.Shared.Controllers;
using UniverCert.Shared.Data.Context;

namespace OffChain.Api.Controllers
{
    public class StageController : BaseController
    {
        private readonly IReadOnlyCatalogService<Stage, int> _service;

        public StageController(IReadOnlyCatalogService<Stage, int> service)
        {
            _service = service;
        }
        /// <summary>
        /// Информаци о ступени обучения
        /// </summary>
        /// <param name="id">Ид ступени обучения </param>
        /// <returns>Информация о ступени обучения</returns>
        [UserRole(RoleEnum.Owner)]
        [HttpGet("get")]
        public async Task<IActionResult> SpecialityInfo(int id)
        {
            try
            {
                if (id == default /*|| CurrentUserRole == RoleEnum.None*/) { return BadRequest("Incorrect stage id"); }
                object _ = await _service.GetInfo(id);
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }


        /// <summary>
        /// Получить весь список специальности
        /// </summary> 
        /// <returns>Информация о специальности</returns>
        [UserRole(RoleEnum.Owner)]
        [HttpGet("all")]
        public async Task<IActionResult> GetAll()
        {
            try
            { 
                object _ = await _service.GetList();
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }
    }
}
