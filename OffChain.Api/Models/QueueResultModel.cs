﻿namespace OffChain.Api.Models
{
    /// <summary>
    /// Модель ответа блокчейн сети для LMS систем
    /// </summary>
    public class QueueResultModel
    {
        public bool Success { get; set; }
        public string Error { get; set; }
        /// <summary>
        /// Ид студента из LMS системы
        /// </summary>
        public int StudentId { get; set; }

        /// <summary>
        /// Номер семестра отправленных оценок
        /// </summary>
        public int Nsem{ get; set; }

        /// <summary>
        ///Хэш выполенной транзакции в блокчейн сети
        /// </summary>
        public string Hash { get; set; }
    }
}
