﻿namespace OffChain.Api.Models
{
    public class StudentSpecialityLinkDto
    {
        /// <summary>
        /// ИД студента  из БД вуза
        /// </summary>
        public int LmsStudentId { get; set; }

        /// <summary>
        /// ИД специальности из справочника вуза
        /// </summary>
        public int LmsSpecialityId { get; set; }
    }
}
