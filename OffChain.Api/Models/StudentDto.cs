﻿using System;
using System.Collections.Generic;
using UniverCert.Shared.Data.Context;

namespace OffChain.Api.Models
{  /// <summary>
   /// Данные студента для API
   /// </summary>
    public class StudentDto
    {
        public StudentDto()
        {
            Specialities = new HashSet<StudentSpecialityLinkDto>();
        }

        /// <summary>
        /// ИИН студента
        /// </summary>
        public string IIN { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string Sname { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string Fname { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime BirthDay { get; set; }

        /// <summary>
        /// Ид студента в LMS системе Вуза
        /// </summary>
        public int LmsStudentId { get; set; }

        /// <summary>
        /// Признак удаления
        /// </summary>
        public bool IsDeleted { get; set; } = false;

        /// <summary>
        /// Специальности студента
        /// </summary>
        public virtual ICollection<StudentSpecialityLinkDto> Specialities { get; set; }
    }
}
