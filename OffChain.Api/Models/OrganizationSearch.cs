﻿namespace OffChain.Api.Models
{
    /// <summary>
    /// Модель для поиска организации
    /// </summary>
    public class OrganizationSearch
    {
        /// <summary>
        /// ИД организации
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование на казахском языке
        /// </summary>
        public string NameKZ { get; set; }

        /// <summary>
        /// Наименование на русском языке
        /// </summary>
        public string NameRU { get; set; }

        /// <summary>
        /// Наименование на английском языке
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Адрес организации
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// БИН организации
        /// </summary>
        public string BIN { get; set; }
        /// <summary>
        /// Страница пагинации
        /// </summary>
        public int? Page { get; set; }

    }
}
