﻿namespace OffChain.Api.Models
{
    /// <summary>
    /// Модель для поиска студентов
    /// </summary>
    public class StudentSearch
    {
        /// <summary>
        /// ИД студента в базе Блокчейн сети
        /// </summary>
        public long StudentId { get; set; }

        /// <summary>
        /// ИД студента в LMS системе  вуза
        /// </summary>
        public int LmsStudentId { get; set; }

        /// <summary>
        /// ИД организации
        /// </summary>
        public int OrganizationId { get; set; }

        /// <summary>
        /// ИИН студента
        /// </summary>
        public string IIN { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string Sname { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string Fname { get; set; }
        
        /// <summary>
        /// Страница пагинации
        /// </summary>
        public int? Page { get; set; }
    }
}
