﻿using Identity.Api.Models;
using System;
using System.Threading.Tasks;

namespace Identity.Api.Services
{
    public interface IAccountService
    {
         /// <summary>
         /// Авторизация пользователя
         /// </summary>
         /// <param name="dto"></param>
         /// <returns>accesToken</returns>
         /// <exception cref="ArgumentException"></exception>
        Task<object> Login(LoginDto dto);

        /// <summary>
        /// Регистрация пользователя
        /// </summary>
        /// <param name="dto"></param>
        /// <returns>id пользователя</returns>
        /// <exception cref="ArgumentException"></exception>
        Task<Guid> Register(RegisterDto dto);
    }
}
