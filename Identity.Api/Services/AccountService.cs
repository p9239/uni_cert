﻿using Identity.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UniverCert.Shared.Data;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Repositories;
using UniverCert.Shared.Data.Variables;
using UniverCert.Shared.Models;
using UniverCert.Shared.Services;

namespace Identity.Api.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUserRepo _userRepo;
        private readonly IMailService _mailService;
        private readonly IOrganizationRepo _organizationRepo;
        private readonly IOptions<AppSettings> _conf;

        public AccountService(IUserRepo userRepo,
            IOptions<AppSettings> conf,
            IMailService mailService,
            IOrganizationRepo organizationRepo)
        {
            _userRepo = userRepo;
            _conf = conf;
            _mailService = mailService;
            _organizationRepo = organizationRepo;
        }
        public async Task<object> Login(LoginDto dto)
        {
            var user = await _userRepo
                .GetQueryable(x => x.Login == dto.Login && !x.IsDeleted)
                .Include(x=>x.Organization)
                .FirstOrDefaultAsync();
            if (user == null)
                throw new ArgumentException("Логин или пароль неправильно");
            if (user.IsBlocked && String.IsNullOrEmpty(user.Password))
                throw new ArgumentException("Ваш аккаунт еще не подтвержден");
            if (user.IsBlocked)
                throw new ArgumentException("Ваш аккаунт заблокирован, обратитесь к администрацию");
            if (user.Password != HashPassword(dto.Password))
            {
                user.PasswordTryCount++;
                if (user.PasswordTryCount >= 5)
                    user.IsBlocked = true;
                await _userRepo.Update(user);
                throw new ArgumentException("Логин или пароль неправильно");
            }
            if(user.PasswordTryCount > 0)
            {
                user.PasswordTryCount = 0;
                await _userRepo.Update(user);
            }
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                    issuer: _conf.Value.AuthOptions.Issuer,
                    audience: _conf.Value.AuthOptions.Audience,
                    notBefore: now,
                    claims: GetClaimsIdentity(user).Claims,
                    expires: now.Add(TimeSpan.FromMinutes(_conf.Value.AuthOptions.Lifetime)),
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_conf.Value.AuthOptions.Key)), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
         
            return new
            {
                accessToken = encodedJwt,
            };
        }

        public async Task<Guid> Register(RegisterDto dto)
        {
            dto.Login = dto.Login?.Trim().ToLower();
            var user = await _userRepo
               .GetQueryable(x => x.Login == dto.Login)
               .AsNoTracking()
               .FirstOrDefaultAsync();
            if(user != null)
                throw new ArgumentException("Пользователь уже зарегистрирован в системе");

            if (!Enum.IsDefined(typeof(OrganizationRole), dto.Organization.Type) || 
                dto.Organization.Type == OrganizationRole.NONE || 
                dto.Organization.Type == OrganizationRole.Ministry)
                throw new ArgumentException("Некорректный тип организации");

            var _organization = _organizationRepo.GetQueryable(x => x.BIN == dto.Organization.BIN).AsNoTracking().FirstOrDefault();
            if (_organization == default)
            {
                _organization = new Organization
                {
                    Id = await _organizationRepo.Add(new Organization
                    {
                        BIN = dto.Organization.BIN,
                        NameEN = dto.Organization.NameEN,
                        NameRU = dto.Organization.NameRU,
                        NameKZ = dto.Organization.NameKZ,
                        Address = dto.Organization.Address,
                        CreatedDate = DateTime.Now,
                        Role = dto.Organization.Type
                    }),
                    Role = dto.Organization.Type
                };
            }

            var _ = new User
            {
                Login = dto.Login,
                Name = dto.Name, 
                OrganizationId = _organization.Id,
                CreatedDate = DateTime.Now,
                Role = _organization.Role == OrganizationRole.Employer ? RoleEnum.Verifier : RoleEnum.Owner,
                IsBlocked = true
            };
            var id = await _userRepo.Add(_);

            string headsMail = await _userRepo.GetQueryable(x => x.Role == RoleEnum.Head)
                    .AsNoTracking()
                    .Select(x => x.Login)
                    .FirstOrDefaultAsync();

            var messageToUser = new MessageMail(
                ToEmail: dto.Login,
                Subject: "Вы успешно зарегистрировали в систему",
                Body: $"Ждите подверждение администрации"
                );

            var messageToHead = new MessageMail(
                ToEmail: headsMail,
                Subject: "Зарегистрирован новый пользователь",
                Body: $"Подвердите пользователя </br> <a href='http://localhost:5000/swagger'>Пройдите по ссылке</a>"
                );

            await _mailService.SendEmailsAsync(messageToHead, messageToUser);

            return id;
        }

        private ClaimsIdentity GetClaimsIdentity(User user)
        {
            if (user != null)
            {
                var claims = new List<Claim>
                {
                   new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                   new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                   new Claim(ClaimsIdentity.DefaultRoleClaimType, Enum.GetName(typeof(RoleEnum),user.Role)),
                   new Claim("organizationId",  user.OrganizationId.ToString()),
                   new Claim("hasRole", Enum.GetName(typeof(RoleEnum),user.Role)),
                   new Claim("name", user.Name),
                };
                if(user.Role == RoleEnum.Owner && user.Organization != default)
                {
                    claims.AddRange(new List<Claim> { 
                        new Claim("orgAccount", user.Organization.BlockChainLogin?.ToString()??""),
                        new Claim("orgPass",  user.Organization.BlockChainPassword?.ToString()??""),
                        new Claim("orgUri",  user.Organization.BlockChainResultUri?.ToString()??""),
                        new Claim("orgSecret",  user.Organization.IntegrationSecret?.ToString()??"")
                    });
                }
                ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }
            return null;
        }
        private string HashPassword(string pwd)
        {
            var alg = SHA512.Create();
            alg.ComputeHash(Encoding.UTF8.GetBytes(pwd));
            string str = Convert.ToBase64String(alg.Hash);
            return str;
        }
    }
}
