﻿using System.Threading.Tasks;
using System;
using Identity.Api.Models;
namespace Identity.Api.Services
{
    public interface IHeadService
    {
        /// <summary>
        /// Получить все не подтвержденные пользователей
        /// </summary>
        /// <param name="dto"></param>
        /// <returns>Список пользователей</returns>
        ///
        Task<object> GetUsers(HeadUserDto dto);
        /// <summary>
        /// Подтверждение пользователя
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Успешность операции</returns>
        Task<object> ConfirmUser(Guid userId);
        /// <summary>
        /// Блокировать или разблокировать пользователя
        /// </summary>
        /// <param name="dto"></param>
        /// <returns>Успешность операции</returns>
        Task<object> BlockingUser(HeadUserBlockingDto dto);
    }
}
