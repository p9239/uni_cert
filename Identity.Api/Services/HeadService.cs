﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using System.Threading.Tasks;
using UniverCert.Shared.Data.Repositories;
using System.Security.Cryptography;
using System.Text;
using UniverCert.Shared.Services;
using UniverCert.Shared.Models;
using Identity.Api.Models;
using UniverCert.Shared.Data.Helpers;
using UniverCert.Shared.Data.Context;
using BlockChain.Api.Services;
using Microsoft.Extensions.Options;
using Nethereum.Web3;
using UniverCert.Shared.Data;

namespace Identity.Api.Services
{
    public class HeadService : IHeadService
    {
        private readonly IUserRepo _userRepo;
        private readonly IMailService _mailService;
        private readonly IOrganizationRepo _organizationRepo;
        private readonly IEncryptionService _encryptionService;
        private readonly IBlockchainService _blockchainService;
        private readonly IOptions<AppSettings> _conf;
        private readonly Web3 Web3Connector; 

        public HeadService(IUserRepo userRepo,
            IMailService mailService,
            IOrganizationRepo organizationRepo,
            IEncryptionService encryptionService,
            IBlockchainService blockchainService, 
            IOptions<AppSettings> conf)
        {
            _userRepo = userRepo;
            _mailService = mailService;
            _organizationRepo = organizationRepo;
            _encryptionService = encryptionService;
            _blockchainService = blockchainService;
            _conf = conf;  
            Web3Connector = new Web3(_conf.Value.BlockChainSettings.OfChainServerUrl);
        }
        public async Task<object> GetUsers(HeadUserDto dto)
        {

            // для генерации тестовых логин паролев
            /*string password = GenerateRandomPassword(12);
            string account = _blockchainService.CreateAccount(password);
            string encryptedPassword = _encryptionService.Encrypt("", "0x86edC3e4B1EF4DdC762837DE98Aedc62A05a1186");
            */

            var _ = _userRepo.Base().Where(x =>
            ((!dto.IsUnconfirmed || dto.IsUnconfirmed && String.IsNullOrEmpty(x.Password)) &&
            (!dto.IsBlocked || dto.IsBlocked && x.IsBlocked && !String.IsNullOrEmpty(x.Password)) ||
            (dto.IsUnconfirmed && dto.IsBlocked && x.IsBlocked)) &&
            (!dto.IsActive || dto.IsActive && !x.IsBlocked) &&
            (dto.Name == default || x.Name.ToLower().StartsWith(dto.Name.ToLower())) &&
            (dto.Login == default || x.Login.ToLower().StartsWith(dto.Login.ToLower())) &&
            !x.IsDeleted && x.Role != RoleEnum.Head
            )
                .Include(x => x.Organization)
                .AsNoTracking()
                .Select(x => new 
                {
                   x.Id,
                   x.Login,
                   x.Name,
                   x.CreatedDate,
                    IsBlocked = x.IsBlocked && !String.IsNullOrEmpty(x.Password),
                    IsUnConfirmed = x.IsBlocked && String.IsNullOrEmpty(x.Password),
                    Organization = new
                    {
                        x.Organization.Id,
                        x.Organization.NameRU,
                        x.Organization.BIN,
                        x.Organization.CreatedDate
                    }
                });
 
            return await PaginatedLists<object>.CreateAsync(_.AsNoTracking(), dto.Page ?? 1, 20);
        }
        public async Task<object> ConfirmUser(Guid userId)
        {
            var _ = await _userRepo.GetQueryable(
                x => x.Id == userId
                && x.IsBlocked
                && String.IsNullOrEmpty(x.Password)
                ).Include(x=>x.Organization).FirstOrDefaultAsync();

            if (_ == default)
                return new ArgumentException("Пользователь не найден или уже подтвержден");
             

            if (_.Role == RoleEnum.Owner && string.IsNullOrEmpty(_.Organization.BlockChainLogin))
            {
                // первичная генерация логина и пароля для организации
                string password = GenerateRandomPassword(12);
                string account = _blockchainService.CreateAccount(password);
                string encryptedPassword = _encryptionService.Encrypt(password, account);
                _.Organization.BlockChainLogin = account;
                _.Organization.BlockChainPassword = encryptedPassword;
                _.Organization.IntegrationSecret = GenerateRandomPassword(12);

                await _organizationRepo.Update(_.Organization); 

                var r = Web3Connector.Personal.UnlockAccount.SendRequestAsync(account, password, 600).Result;// Активируем созданный аккаунт для организации
            }
            var (ReadablePassword, HashPassword) = GetPassword();

            _.Password = HashPassword;
            _.IsBlocked = false;
           await _userRepo.Update(_);

          

            await _mailService.SendEmailAsync(new MessageMail(
                ToEmail: _.Login,
                Subject: "Ваш аккаунт успешно подтверждено",
                Body: $"Доступ к системе <br/><strong>Логин: </strong>{_.Login} <br/><strong>Пароль: </strong>{ReadablePassword}"
                ));

            return true;
        }
        public async Task<object> BlockingUser(HeadUserBlockingDto dto)
        {
            var _ = await _userRepo.GetQueryable(
                x => x.Id == dto.UserId && x.IsBlocked != dto.IsBlocked).FirstOrDefaultAsync();
            string blockTxt = (dto.IsBlocked ? "заблокирован" : "разблокирован");
            if (_ == default)
                return new ArgumentException("Пользователь не найден или уже " + blockTxt);

          
            _.IsBlocked = dto.IsBlocked;
            await _userRepo.Update(_);

            await _mailService.SendEmailAsync(new MessageMail(
                ToEmail: _.Login,
                Subject: null,
                Body: $"Ваш аккаунт " + blockTxt
                ));

            return true;
        }
        private (string ReadablePassword, string HashPassword) GetPassword()
        {
            string pwd = GenerateRandomPassword();

            //Получение SHA512 хэш
            var alg = SHA512.Create();
            alg.ComputeHash(Encoding.UTF8.GetBytes(pwd));
            string hash = Convert.ToBase64String(alg.Hash);

            return (pwd, hash);
        }

        private string GenerateRandomPassword(int minSize = 8)
        {
            if (minSize < 8)
                throw new ArgumentException("Password valid minSize is 8");

            string validChars = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*?_";
            int validCharsLength = validChars.Length-1;
            Random random = new Random();
            int size = random.Next(minSize, 16);
            char[] chars = new char[size];
            for (int i = 0; i < size; i++)
            {
                chars[i] = validChars[random.Next(0, validCharsLength)];
            }
            var pwd = new string(chars);

            return pwd;
        }
    }
}
