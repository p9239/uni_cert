﻿using Autofac;
using BlockChain.Api.Services;
using System.Reflection;
using UniverCert.Shared.Data.Repositories;
using UniverCert.Shared.Services;

namespace Identity.Api
{
    public class AutofacModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = Assembly.Load(new AssemblyName("Identity.Api"));
            builder.RegisterAssemblyTypes(assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            builder.RegisterModule<BlockChain.Api.AutofacModule>();

            //repos
            builder.RegisterType<UserRepo>().As<IUserRepo>().InstancePerLifetimeScope();
            builder.RegisterType<OrganizationRepo>().As<IOrganizationRepo>().InstancePerLifetimeScope();

            //services
            builder.RegisterType<MailService>().As<IMailService>().InstancePerLifetimeScope();
            builder.RegisterType<EncryptionService>().As<IEncryptionService>().InstancePerLifetimeScope();
         }
    }
}
