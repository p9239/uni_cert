﻿using Identity.Api.Services;
using Microsoft.AspNetCore.Mvc;
using UniverCert.Shared;
using UniverCert.Shared.Controllers;
using UniverCert.Shared.Data.Context;
using System.Threading.Tasks;
using System;
using Identity.Api.Models;

namespace Identity.Api.Controllers
{
    [UserRole(new RoleEnum[] {  RoleEnum.Head /*, RoleEnum.Owner*/})]
    public class HeadController : BaseController
    {
        private readonly IHeadService _headService;
        public HeadController(IHeadService headService)
        {
            _headService = headService;
        }
        /// <summary>
        /// Получить все не подтвержденние пользователей
        /// </summary>
        /// <returns>Список пользователей</returns>
        [HttpGet("users")]
        public async Task<IActionResult> GetUsers([FromQuery] HeadUserDto dto)
        {
            try
            {
                var _ = await _headService.GetUsers(dto);
                return Ok(_);
            }
            catch(Exception e)
            {
                return ExceptionResult(e);
            }
        }
        /// <summary>
        /// Подтверждение пользователя
        /// </summary>
        /// <param name="dto"></param>
        /// <returns>Успешность операции</returns>
        [HttpPost(nameof(ConfirmUser))]
        public async Task<IActionResult> ConfirmUser([FromBody] HeadDto dto)
        {
            try
            {
                if(dto.UserId == default)
                    return BadRequest("Введите id пользователя");
                var _ = await _headService.ConfirmUser(dto.UserId);
                return Ok(new {success = _ });
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }

        /// <summary>
        /// Подтверждение пользователя
        /// </summary>
        /// <param name="dto"></param>
        /// <returns>Успешность операции</returns>
        [HttpPost("blockinguser")]
        public async Task<IActionResult> BlockingUser([FromBody] HeadUserBlockingDto dto)
        {
            try
            {
                if (dto.UserId == default)
                    return BadRequest("Введите id пользователя");
                var _ = await _headService.BlockingUser(dto);
                return Ok(new { success = _ });
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }
    }
}
