﻿using Identity.Api.Models;
using Identity.Api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using UniverCert.Shared.Controllers;

namespace Identity.Api.Controllers
{
    [AllowAnonymous]
    
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
           _accountService = accountService;
        }
        /// <summary>
        /// Авторизация пользователья 
        /// </summary>
        /// <response code="200">успешный запрос</response>
        /// <response code="400">модел невалидный или пользователь не найден</response>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginDto login)
        {
            try
            {  
                if (!ModelState.IsValid) return BadRequest(ModelState);
                var _ = await _accountService.Login(login);
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }
        /// <summary>
        /// Регистрация пользователя
        /// </summary>
        /// <param name="registerDto"></param>
        /// <returns>id пользователя</returns>
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterDto registerDto)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                var _ = await _accountService.Register(registerDto);
                return Ok(_);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }
    }
}
