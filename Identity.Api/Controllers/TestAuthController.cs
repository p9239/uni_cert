﻿using BlockChain.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using UniverCert.Shared;
using UniverCert.Shared.Controllers;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Helpers;

namespace Identity.Api.Controllers
{

    [Authorize(Roles = "Owner")]
    [UserRole(RoleEnum.Owner)]
    public class TestAuthController : BaseController
    {
        /// <summary>
        /// Тест на авторизация рол:Owner
        /// </summary>
        /// <returns></returns>
        [HttpGet("Testing")]
        public object Get()
        {
            return new  { CurrentUserId,CurrentUserOrganizationId}; 
        }
        [AllowAnonymous]
        [HttpPost("SaveBlockResult")]
        public object SaveBlockResult()
        {
            string token = null;
            var authorizationHeader = Request.Headers["Authorization"];
            var authorizationParts = authorizationHeader.ToString().Split(' ');
            if (authorizationParts.Length == 2 && authorizationParts[0] == "Bearer")
            {
                token = authorizationParts[1];
            }

            BlockChainResult json = null;
            try
            {
               json = SharedHelper.DecodeJWTToken<BlockChainResult>(token, "hereIsToken");
            }
            catch (Exception ex)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return BadRequest(new { StatusCode = (int)HttpStatusCode.InternalServerError, mess = ex.Message });
            }


            return json;
        }
    }
}
