﻿using System.ComponentModel.DataAnnotations;

namespace Identity.Api.Models
{
    public class LoginDto
    {
        [Required(ErrorMessage = "Обязательно для заполнения")]
        public string Login { get; set; }
        [Required(ErrorMessage = "Обязательно для заполнения")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
