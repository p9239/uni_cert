﻿using System.ComponentModel.DataAnnotations;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Variables;

namespace Identity.Api.Models
{
    public class RegisterDto
    {
        [Required(ErrorMessage = "Обязательно для заполнения")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Введите корректный e-mail")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Обязательно для заполнения")]
        public string Name { get; set; }
        public OrganizationDto Organization { get; set; }
    }

    public class OrganizationDto
    {
        /// <summary>
        /// Наименование на казахском языке
        /// </summary>
        public string NameKZ { get; set; }

        /// <summary>
        /// Наименование на русском языке
        /// </summary>
        public string NameRU { get; set; }

        /// <summary>
        /// Наименование на английском языке
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Адрес организации
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Тип организации
        /// </summary>
        public OrganizationRole Type { get; set; }

        /// <summary>
        /// БИН организации
        /// </summary>
        [Required(ErrorMessage = "Обязательно для заполнения")]
        [RegularExpression(@"^\d{12}$", ErrorMessage = "Введите корректный БИН")]
        public string BIN { get; set; }
    }
}
