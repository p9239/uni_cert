﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Identity.Api.Models
{
    public class HeadDto
    {
        [Required(ErrorMessage = "Обязательно для заполнения")]
        public Guid UserId { get; set; }
    }
    public class HeadUserDto
    { 
        public bool IsBlocked { get; set; }
        public bool IsUnconfirmed { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        /// <summary>
        /// Страница пагинации
        /// </summary>
        public int? Page { get; set; }
    }
    public class HeadUserBlockingDto : HeadDto
    {
        public bool IsBlocked { get; set;}
    }
}
