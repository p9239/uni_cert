using Autofac;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UniverCert;
using UniverCert.Shared.Data;

namespace Identity.Api
{
    public class Startup : StartupShared
    {
        public Startup(IConfiguration configuration, IHostEnvironment hosting) : base(configuration, hosting) { }

        public void ConfigureServices(IServiceCollection services){
            base.ConfigureServices(services);   
            var bus = Bus.Factory.CreateUsingRabbitMq();
            services.AddSingleton<IBusControl>(Bus.Factory.CreateUsingRabbitMq());
        }
        public void ConfigureContainer(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterModule<AutofacModule>();
            base.ConfigureContainer(containerBuilder);
        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory logger) => base.Configure(app, env,logger);
    }
}
