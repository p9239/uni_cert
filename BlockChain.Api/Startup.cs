using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting; 
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using UniverCert;
using MassTransit;
using UniverCert.Shared.Data;
using BlockChain.Api.Consumers;

namespace BlockChain.Api
{
    public class Startup : StartupShared
    {
        private readonly IConfiguration _configuration;
        private string _transactionInsertQueueName = "blockChain.insertTransaction";
        protected static IHostEnvironment _host;
        public Startup(IConfiguration configuration, IHostEnvironment hosting) : base(configuration, hosting) {
            _configuration = configuration;
            _host = hosting;
            _transactionInsertQueueName = _host.EnvironmentName + ":" + _transactionInsertQueueName;
        }

        public void ConfigureServices(IServiceCollection services) {
            base.ConfigureServices(services);
            services.AddMassTransit(x =>
            {
                x.AddConsumer<TransactionInsertConsumer>().Endpoint(e => {
                    e.Name = _transactionInsertQueueName;
                    e.PrefetchCount = 1;
                    e.Temporary = false;
                });
                x.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cur =>
                {
                var _busSettings = Configuration.GetSection("AppSettings:RabbitMqSettings").Get<RabbitMqSettings>();

                    cur.Host(new Uri(_busSettings.Host), h =>
                    {
                        h.Username(_busSettings.Username);
                        h.Password(_busSettings.Password);
                    });

                    cur.MessageTopology.SetEntityNameFormatter(new EnvironmentNameFormatter(cur.MessageTopology.EntityNameFormatter));

                    cur.ReceiveEndpoint(_transactionInsertQueueName, oq =>
                        {
                            oq.PrefetchCount = 1;
                            oq.UseMessageRetry(r => r.Interval(2, 100));
                            oq.ConfigureConsumer<TransactionInsertConsumer>(provider);
                        });

                    }));
            });
        
            services.AddHostedService<BackgroundServices.MassTransitHostedService>();

        }

        public void ConfigureContainer(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterModule<AutofacModule>();
            base.ConfigureContainer(containerBuilder);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,ILoggerFactory logger) => base.Configure(app, env,logger);

        class EnvironmentNameFormatter : IEntityNameFormatter
        {
            private IEntityNameFormatter _original;
            public EnvironmentNameFormatter(IEntityNameFormatter original)
            {
                _original = original;
            }

            public string FormatEntityName<T>()
            {
                var environment = _host.EnvironmentName;
                var name = $"{environment}:{_original.FormatEntityName<T>()}";
                return name;
            }
        }
    }

}