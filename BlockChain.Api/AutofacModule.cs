﻿using System.Reflection;
using Autofac;
using BlockChain.Api.Services;
using UniverCert.Shared.Data.Repositories;
using UniverCert.Shared.Data.Repositories.ControllType.cs;
using UniverCert.Shared.Services;

namespace BlockChain.Api
{
    public class AutofacModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = Assembly.Load(new AssemblyName("BlockChain.Api"));
            builder.RegisterAssemblyTypes(assembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<SubjectRepo>().As<ISubjectRepo>().InstancePerLifetimeScope();
            builder.RegisterType<SpecialityRepo>().As<ISpecialityRepo>().InstancePerLifetimeScope();
            builder.RegisterType<StudentRepo>().As<IStudentRepo>().InstancePerLifetimeScope();
            builder.RegisterType<ControllTypeRepo>().As<IControllTypeRepo>().InstancePerLifetimeScope();
            builder.RegisterType<EncryptionService>().As<IEncryptionService>().InstancePerLifetimeScope();

        }
    }
}
