﻿using BlockChain.Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlockChain.Api.Services
{
    public interface IBlockchainService
    {
        Task<object> InsertBlock(StudDataDto studData, string account, string password, string resultUrl, string secret, int organizationId);
        List<StudDataDto> GetStudDataDtoFromBlock(int studId);
        List<RetJSONClass> GetStudentDtoWithSpecificSpecialityFromBlock(int studId, int specialityId);
        string CreateAccount(string password);
    }
}
