﻿using Nethereum.Web3;
using System;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.Web3.Accounts;
using BlockChain.Api.Models;
using System.Collections.Generic;
using System.Linq;
using UniverCert.Shared.Data.Repositories;
using UniverCert.Shared.Data.Repositories.ControllType.cs;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Text.Json;
using UniverCert.Shared.Data.Context;
using System.Diagnostics;
using Serilog;
using UniverCert.Shared.Data;
using Microsoft.Extensions.Options;
using MassTransit;

namespace BlockChain.Api.Services
{
    public class BlockchainService : IBlockchainService
    {
        private readonly ISubjectRepo _subjectRepo;
        private readonly ISpecialityRepo _specialityRepo;
        private readonly IStudentRepo _studentRepo;
        private readonly IControllTypeRepo _controllTypeRepo;
        private readonly IOptions<AppSettings> _conf;
        private readonly Web3 Web3Connector;
        private readonly string ABI;
        private readonly string ContractAddress;
        private readonly IPublishEndpoint _publishEndpoint;
        public BlockchainService(
            IControllTypeRepo controllTypeRepo,
            IStudentRepo studentRepo,
            IOptions<AppSettings> conf,
            ISpecialityRepo specialityRepo,
            ISubjectRepo subjectRepo,
            IPublishEndpoint publishEndpoint)
        {
            _controllTypeRepo = controllTypeRepo;
            _specialityRepo = specialityRepo;
            _studentRepo = studentRepo;
            _subjectRepo = subjectRepo;
            _conf = conf;
            _publishEndpoint = publishEndpoint;


            ABI = _conf.Value.BlockChainSettings.ABI; 
            ContractAddress = _conf.Value.BlockChainSettings.ContractAddress;
            Web3Connector = new Web3(_conf.Value.BlockChainSettings.OfChainServerUrl);
        }

        public async Task<object> InsertBlock(StudDataDto studData, string account, string password, string resultUrl, string secret, int organizationId)
        {  

            #region Проверка данных

            var comStudentId = await _studentRepo.Base()
                .Include(l => l.Specialities).ThenInclude(y => y.Speciality)
                .Where(x => x.LmsStudentId == studData.StudId &&
                x.Specialities.Select(o => o.Speciality.OrganizationId).Contains(organizationId))
                .AsNoTracking().Select(x => x.Id).FirstOrDefaultAsync();
            if (comStudentId == default)
                return new ArgumentException("Студент не найден");

            var comSpecId = await _specialityRepo.Base().Where(x => x.LmsSpecId == studData.SpecialityId && x.OrganizationId == organizationId)
                .AsNoTracking().Select(x => x.Id).FirstOrDefaultAsync();
            if (comSpecId == default)
                return new ArgumentException("Специальность не найден");

            var comSubjects = await _subjectRepo.Base()
                .Where(x => studData.JSON.Select(y => y.LMSSubjectId).Contains(x.LmsSubjectId) && x.OrganizationId == organizationId)
                .Distinct().AsNoTracking().Select(x => new Subject { Id = x.Id, LmsSubjectId = x.LmsSubjectId }).ToListAsync();

            if (comSubjects.Count() <= 0)
                return new ArgumentException("Дисциплины не найдены");

            var comControlTypeIds = await _controllTypeRepo.GetQueryable(
                 x => studData.JSON.Select(y => y.ControlTypeId).Contains(x.Id))
                .Distinct().AsNoTracking().Select(x => x.Id).ToListAsync();

            if (comControlTypeIds.Count() <= 0)
                return new ArgumentException("Тип контролы не найдены");

            var notFoundSubjects = new List<long>();
            var notFoundControlTypes = new List<int>();
            foreach (var s in studData.JSON)
            {
                var _comSubject = comSubjects.Where(x => x.LmsSubjectId == s.LMSSubjectId).FirstOrDefault();
                if (_comSubject != default)
                    s.LMSSubjectId = _comSubject.Id;
                else
                    notFoundSubjects.Add(s.LMSSubjectId);

                var _comControlTypeId = comControlTypeIds.Where(x => x == s.ControlTypeId).FirstOrDefault();
                if (_comControlTypeId != default)
                    s.ControlTypeId = _comControlTypeId;
                else
                    notFoundControlTypes.Add(s.ControlTypeId);
            }
            if (notFoundSubjects.Count() > 0)
                return new ArgumentException($"Следующие дисциплины не найдены {String.Join(',', notFoundSubjects)}");
            if (notFoundControlTypes.Count() > 0)
                return new ArgumentException($"Следующие тип контролы не найдены {String.Join(',', notFoundControlTypes)}");

            studData.StudId = comStudentId;
            studData.SpecialityId = comSpecId;
            studData.JsonData = JsonSerializer.Serialize(studData.JSON);
            //studData.CreatedDate = DateTime.Now.ToString("yyMMdd");
            #endregion
            if (password == null) password = "";
            studData.JsonData.Replace("'", "`");
            studData.JsonData.Replace("\"", "`");

            await _publishEndpoint.Publish<TransactionInsertMessage>(new TransactionInsertMessage
            {
                JsonData = studData.JsonData,
                Nsem = studData.JSON[0].Nsem,
                StudId = studData.StudId,
                SpecialityId = studData.SpecialityId,
                Account = account,
                Password = password,
                ResultUrl = resultUrl,
                Secret = secret,
            });

            return new { success = true };

        }

        public List<StudDataDto> GetStudDataDtoFromBlock(int studDataId)
        {
            var Contract = Web3Connector.Eth.GetContract(ABI, ContractAddress);
            var GetDataFunction = Contract.GetFunction("findStudentsById");
            var retval = GetDataFunction.CallAsync<List<OutputDto>>(studDataId).Result;
            List<StudDataDto> returnValue = StudDataDto.GetStudDataFromOutputDto(studDataId, retval);

            return returnValue;
        }

        public List<RetJSONClass> GetStudentDtoWithSpecificSpecialityFromBlock(int studId, int speacialityId)
        {
            List<StudDataDto> studData = this.GetStudDataDtoFromBlock(studId);
            List<StudDataDto> retVal = studData.Where(x => x.SpecialityId == speacialityId).Select(x => x).ToList();
            List<RetJSONClass> ret = new List<RetJSONClass>();
            foreach (var item in retVal)
            {
                List<JSONClass> js = JsonSerializer.Deserialize<List<JSONClass>>(item.JsonData);
                foreach (JSONClass json in js)
                {
                    var subject = _subjectRepo.Base().Where(x => x.Id == json.LMSSubjectId).Select(x => x.NameRU).FirstOrDefault();
                    var controlType = _controllTypeRepo.Base().Where(x => x.Id == json.ControlTypeId).Select(x => x.NameRU).FirstOrDefault();
                    ret.Add(new RetJSONClass
                    {
                        //CreatedDate = Int32.Parse(item.CreatedDate),
                        ControlType = controlType,
                        LMSSubjectName = subject,
                        MarkSymbol = json.MarkSymbol,
                        Mark = json.Mark,
                        Credit = json.Credit,
                        Nsem = json.Nsem

                    });
                }
            }
            return ret.OrderBy(x => x.Nsem).ToList();
        }

        public string CreateAccount(string password)
        {
            if (password == null) password = "";
            var acc = Web3Connector.Personal.NewAccount.SendRequestAsync(password).Result;
            return acc;
        }
    }
}
