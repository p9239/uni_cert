﻿using BlockChain.Api.Models;
using MassTransit;
using Microsoft.Extensions.Options;
using Nethereum.Hex.HexTypes;
using Nethereum.Web3;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using UniverCert.Shared.Data;
using UniverCert.Shared.Data.Helpers;
using UniverCert.Shared.Services;

namespace BlockChain.Api.Consumers
{
    public class TransactionInsertConsumer : IConsumer<TransactionInsertMessage>
    {
        private readonly Web3 Web3Connector;
        private readonly string ABI;
        private readonly string ContractAddress;
        private readonly IOptions<AppSettings> _conf;
        private IEncryptionService _encryptionService;

        public TransactionInsertConsumer(IOptions<AppSettings> options,IEncryptionService encryptionService)
        {
            _conf = options;
            _encryptionService = encryptionService;

            ABI = _conf.Value.BlockChainSettings.ABI;
            ContractAddress = _conf.Value.BlockChainSettings.ContractAddress;
            Web3Connector = new Web3(_conf.Value.BlockChainSettings.OfChainServerUrl);
        }
        public async Task Consume(ConsumeContext<TransactionInsertMessage> context)
        {
            BlockChainResult result = new BlockChainResult();
            try
            {
                TransactionInsertMessage _ = context.Message;
                var Password = _encryptionService.Decrypt(encryptedText: _.Password, key: _.Account);
                var watch = Stopwatch.StartNew();
                
                var Contract = Web3Connector.Eth.GetContract(ABI, ContractAddress);
                var InsertDataFunction = Contract.GetFunction("addStudData");
                /*var r = await Web3Connector.Personal.UnlockAccount.SendRequestAsync(_.Account,Password, 600);
                if (!r)
                     throw new Exception("Authorization failed");*/
                var retvalInsert = await InsertDataFunction.SendTransactionAndWaitForReceiptAsync(_.Account,
                     new HexBigInteger(new BigInteger(3000000)), new HexBigInteger(new BigInteger(0)), null, new object[] { _.SpecialityId, _.StudId, _.JsonData });

                watch.Stop();
                result.Success = true;
                result.Hash = retvalInsert.BlockHash;
                result.StudentId = context.Message.StudId;
                result.Nsem = context.Message.Nsem;
      
                Log.Information("{MethodName} {hash} {time}", "blockchainTransaction", retvalInsert.TransactionHash, watch.ElapsedMilliseconds);
            }
            catch(Exception ex)
            {
                Log.Error("{MethodName} {Error}", "blockchainTransactionError", ex.Message);
                result.Success = false;
                result.Error = ex.Message;
            }
            finally
            {
                try
                {
                    var client = new HttpClient();

                    Dictionary<string,object> dict = new Dictionary<string,object>();
                    dict["Hash"] = result.Hash;
                    dict["StudentId"] = result.StudentId;
                    dict["Nsem"] = result.Nsem;
                    dict["Error"] = result.Error;
                    dict["Success"] = result.Success;

                    string token = SharedHelper.MakeJWTToken(dict, context.Message.Secret);
                    
                    client.DefaultRequestHeaders.Add(HttpRequestHeader.Authorization.ToString(), $"Bearer {token}");
                    await client.PostAsync(context.Message.ResultUrl,
                        new StringContent(JsonConvert.SerializeObject(result), Encoding.UTF8, "application/json"));
                }
                catch(Exception ex)
                {
                    Log.Error("{MethodName} {Account} {Error}", "transactionSendResultError", context.Message.Account, ex.Message);
                }
            }
        }
    }
}
