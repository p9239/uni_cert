﻿namespace BlockChain.Api.Models
{
    public class TransactionInsertMessage
    {
        public long StudId { get; set; }
        public long SpecialityId { get; set; }
        public string JsonData { get; set; }
        public int Nsem { get; set; }
        public string ResultUrl { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public string Secret { get; set; }
    }
}
