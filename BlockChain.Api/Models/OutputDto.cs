﻿using Nethereum.ABI.FunctionEncoding.Attributes;

namespace BlockChain.Api.Models
{
    [FunctionOutput]
    public class OutputDto : IFunctionOutputDTO
    {
        [Parameter("uint","specialityId")]
        public virtual int SpecialityId { get; set; }
        [Parameter("string", "jsonData")]
        public virtual string JsonData { get; set; }
    }

}
