﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace BlockChain.Api.Models
{
    public class StudDataDto
    {
        public StudDataDto()
        {

        }
        public StudDataDto(int studentId, OutputDto output)
        {
            this.StudId = studentId;

            this.SpecialityId=output.SpecialityId;
            this.JsonData = output.JsonData.Replace("`", "'");
        }
        public static List<StudDataDto> GetStudDataFromOutputDto (int studentId, List<OutputDto> output)
        {
            List<StudDataDto> retVal = new List<StudDataDto> ();
            output.ForEach(x=> retVal.Add(new StudDataDto(studentId, x)));
            return retVal;
        }

        [Required(ErrorMessage = "Обязательно для заполнения")]
        public long StudId { get; set; }

        [Required(ErrorMessage = "Обязательно для заполнения")]
        public long SpecialityId { get; set; }

        [JsonIgnore]
        public string JsonData { get; set; }

        [Required(ErrorMessage = "Обязательно для заполнения")]
        public List<JSONClass> JSON { get; set; }
       // [JsonIgnore]
        //public string CreatedDate { get; set; }
    }
    public class JSONClass
    {
        [Required(ErrorMessage = "Обязательно для заполнения")]
        public long LMSSubjectId { get; set; }

        [Required(ErrorMessage = "Обязательно для заполнения")]
        public int Mark { get; set; }

        [Required(ErrorMessage = "Обязательно для заполнения")]
        public string MarkSymbol { get; set; } 

        [Required(ErrorMessage = "Обязательно для заполнения")]
        public int Credit { get; set; }

        [Required(ErrorMessage = "Обязательно для заполнения")]
        public int ControlTypeId { get; set; }

        [Required(ErrorMessage = "Обязательно для заполнения")]
        public int Nsem { get; set; }
    }
    public class RetJSONClass
    {
        public string LMSSubjectName { get; set; }
        public int Mark { get; set; }
        public string MarkSymbol { get; set; }
        public int Credit { get; set; }
        public string ControlType { get; set; }
        public int Nsem { get; set; }
       // public int CreatedDate { get; set; }
    }

    public class StudentSpecialityDto
    {
        public int StudentId { get; set; }
        public int SpecialityId { get; set; }
    }
}
