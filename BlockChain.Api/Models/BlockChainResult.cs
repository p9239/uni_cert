﻿namespace BlockChain.Api.Models
{
    public class BlockChainResult
    {
        public bool Success { get; set; }
        public string Error { get; set; }
        /// <summary>
        /// Ид студента из LMS системы
        /// </summary>
        public long StudentId { get; set; }

        /// <summary>
        /// Номер семестра отправленных оценок
        /// </summary>
        public int Nsem { get; set; }

        /// <summary>
        ///Хэш выполенной транзакции в блокчейн сети
        /// </summary>
        public string Hash { get; set; }


    }
}
