﻿using BlockChain.Api.Models;
using BlockChain.Api.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using UniverCert.Shared;
using UniverCert.Shared.Controllers;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Services;

namespace BlockChain.Api.Controllers
{
    
    public class BlockOperationsController : BaseController
    {
        private IBlockchainService _blockChainService; 
        private IEncryptionService _encryptionService; 

        public BlockOperationsController(IBlockchainService blockChainService, IEncryptionService encryptionService)
        {
            _blockChainService = blockChainService; 
            _encryptionService = encryptionService;
        }
        [UserRole(RoleEnum.Owner)]
        [HttpPost("insert")]
        public async Task<IActionResult> InsertDataIntoBlock([FromBody] StudDataDto st/*, string account, string password*/)
        {
            try
            {
                if (BlockChainAccount == default)
                    throw new ArgumentNullException("Организация не найден в бд Блокчейн");
                //TODO: в этом методе нужно убрать account и password
                //получить account и password из данных организации текущего пользователя {CurrentUserOrganizationId}

                //Здесь account и password нужно брать из данных организации текущего пользователя
               
                string account = BlockChainAccount.Login;
                string password = BlockChainAccount.Password; //_encryptionService.Decrypt(encryptedText: BlockChainAccount.Password, key: account);

                var ret = await _blockChainService.InsertBlock(st, account, password, BlockChainAccount.Uri,BlockChainAccount.Secret, CurrentUserOrganizationId);
                return Ok(ret);
            }
            catch(Exception e)
            {
                return ExceptionResult(e);
            }
        }

        [HttpPost("get_students")]
        public IActionResult GetStudentsFromBlock(int studentId)
        {
            try
            {
                var ret = _blockChainService.GetStudDataDtoFromBlock(studentId);
                return Ok(ret);
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpPost("get_students_by_speciality")]
        public IActionResult GetStudentsFromBlockBySpecialityId ([FromBody] StudentSpecialityDto dto)
        {
            try
            {
                var list = _blockChainService.GetStudentDtoWithSpecificSpecialityFromBlock(dto.StudentId, dto.SpecialityId);
                return Ok(list);
            }
            catch(Exception e)
            {
                return ExceptionResult(e);
            }
        }
        [HttpPost("create_account")]
        public IActionResult CreateAccount (string password)
        {
            try
            {
                var ret = _blockChainService.CreateAccount(password);
                return Ok(ret);
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
