﻿using MassTransit;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace BlockChain.Api.BackgroundServices
{
    public class MassTransitHostedService : IHostedService
    {
        private readonly IBusControl _bus;
        public MassTransitHostedService(IBusControl bus)
        {
            _bus = bus;
        }

      /*  public void Dispose()
        {
            if( _bus.CheckHealth().Status == BusHealthStatus.Healthy)
                _bus.Stop();
            
            Log.Warning($"Message broker Disposeed: {DateTime.Now}");
        }*/

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            if (_bus.CheckHealth().Status != BusHealthStatus.Healthy)
            {
              Log.Warning($"Message broker started: {DateTime.Now}");
              await _bus.StartAsync(cancellationToken).ConfigureAwait(false);
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            if (_bus.CheckHealth().Status == BusHealthStatus.Healthy)
            {
                Log.Warning($"Message broker stopped: {DateTime.Now}");
                await _bus.StopAsync(cancellationToken);
            }
        }
    }
}
