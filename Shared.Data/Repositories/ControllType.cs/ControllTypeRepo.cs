﻿using UniverCert.Shared.Data.Context;

namespace UniverCert.Shared.Data.Repositories.ControllType.cs
{
    public class ControllTypeRepo : BaseRepo<Context.ControllType, int>, IControllTypeRepo
    {
        public ControllTypeRepo(DataContext context) : base(context)
        {

        }
    }
}
