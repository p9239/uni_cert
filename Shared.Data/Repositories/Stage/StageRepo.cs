﻿using UniverCert.Shared.Data.Context;

namespace UniverCert.Shared.Data.Repositories.Stage
{
    public class StageRepo : BaseRepo<Context.Stage, int>, IStageRepo
    {
        public StageRepo(DataContext context) : base(context)
        {

        }
    }
}
