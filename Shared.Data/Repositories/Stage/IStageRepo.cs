﻿namespace UniverCert.Shared.Data.Repositories.Stage
{
    public interface IStageRepo : IBaseRepo<Context.Stage, int>
    {
    }
}
