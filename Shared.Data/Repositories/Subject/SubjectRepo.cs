﻿using UniverCert.Shared.Data.Context;

namespace UniverCert.Shared.Data.Repositories
{
    public class SubjectRepo : BaseRepo<Subject, long>, ISubjectRepo
    { 
        public SubjectRepo(DataContext context) : base(context)
        {

        }
    }
}
