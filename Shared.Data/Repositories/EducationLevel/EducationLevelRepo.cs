﻿using UniverCert.Shared.Data.Context;

namespace UniverCert.Shared.Data.Repositories.EducationLevel
{
    public class EducationLevelRepo : BaseRepo<Context.EducationLevel, int>, IEducationLevelRepo
    {
        public EducationLevelRepo(DataContext context) : base(context)
        {

        }
    }
}
