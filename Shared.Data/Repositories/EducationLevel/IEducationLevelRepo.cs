﻿namespace UniverCert.Shared.Data.Repositories.EducationLevel
{
    public interface IEducationLevelRepo : IBaseRepo<Context.EducationLevel, int>
    {
    }
}
