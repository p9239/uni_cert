﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace UniverCert.Shared.Data.Repositories
{
    public interface IBaseRepo<TEntity, T>
    {
        IQueryable<TEntity> Base();
        IQueryable<TEntity> GetQueryable(Expression<Func<TEntity, bool>> expression);
        Task<T> Add(TEntity entity);
        Task AddRange(IEnumerable<TEntity> entity);
        Task Save();
        Task<TEntity> GetById(T id); 
        Task Delete(TEntity entity);
        Task DeleteRange(IEnumerable<TEntity> entities);
        Task Remove(TEntity entity);
        Task RemoveRange(IEnumerable<TEntity> entities);
        Task Update(TEntity entity);
        Task UpdateRange(IEnumerable<TEntity> entity);
        ICollection<TEntity> GetAll();
    }
}
