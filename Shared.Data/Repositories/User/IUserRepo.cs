﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniverCert.Shared.Data.Repositories
{
    public interface IUserRepo : IBaseRepo<Context.User,Guid>
    {
    }
}
