﻿using System;
using System.Collections.Generic;
using System.Text;
using UniverCert.Shared.Data.Context;

namespace UniverCert.Shared.Data.Repositories
{
    public class UserRepo : BaseRepo<Context.User,Guid>,IUserRepo
    {
        public UserRepo(DataContext context):base(context)
        {

        }
    }
}
