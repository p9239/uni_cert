﻿using Microsoft.EntityFrameworkCore;
using UniverCert.Shared.Data.Context;

namespace UniverCert.Shared.Data.Repositories
{
    public class OrganizationRepo : BaseRepo<Context.Organization, int>, IOrganizationRepo
    { 
        public OrganizationRepo(DataContext context) : base(context)
        {

        }
    }
}
