﻿namespace UniverCert.Shared.Data.Repositories
{
    public interface IOrganizationRepo : IBaseRepo<Context.Organization, int>
    {
    }
}
