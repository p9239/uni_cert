﻿namespace UniverCert.Shared.Data.Repositories
{
    public interface ISpecialityRepo : IBaseRepo<Context.Speciality, long>
    { 
    }
}
