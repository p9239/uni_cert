﻿using UniverCert.Shared.Data.Context;

namespace UniverCert.Shared.Data.Repositories
{
    public class SpecialityRepo : BaseRepo<Speciality, long>, ISpecialityRepo
    { 
        public SpecialityRepo(DataContext context) : base(context)
        {

        }
    }
}
