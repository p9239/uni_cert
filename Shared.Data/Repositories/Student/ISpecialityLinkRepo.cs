﻿namespace UniverCert.Shared.Data.Repositories.Student
{
    public interface IStudentSpecialityLinkRepo : IBaseRepo<Context.StudentSpecialityLink, long>
    {
    }
}
