﻿namespace UniverCert.Shared.Data.Repositories
{
    public interface IStudentRepo : IBaseRepo<Context.Student, long>
    {
    }
}
