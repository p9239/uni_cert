﻿using UniverCert.Shared.Data.Context;

namespace UniverCert.Shared.Data.Repositories
{
    public class StudentRepo : BaseRepo<Context.Student, long>, IStudentRepo
    {
        public StudentRepo(DataContext context) : base(context)
        {

        } 
    }
}
