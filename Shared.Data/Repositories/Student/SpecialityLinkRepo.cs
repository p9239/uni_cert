﻿using UniverCert.Shared.Data.Context;

namespace UniverCert.Shared.Data.Repositories.Student
{
    public class StudentSpecialityLinkRepo : BaseRepo<Context.StudentSpecialityLink, long>, IStudentSpecialityLinkRepo
    {
        public StudentSpecialityLinkRepo(DataContext context) : base(context)
        {

        }
    }
}
