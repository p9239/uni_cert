﻿using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.ComponentModel;
using System.Linq;


namespace UniverCert.Shared.Data.Variables
{
    /// <summary>
    /// Роли организации в блокчейн сети
    /// </summary>
    public enum OrganizationRole
    {
        /// <summary>
        /// Без роли
        /// </summary>
        NONE = 0,

        /// <summary>
        /// Университет
        /// </summary>
        [Descriptor("Университет")]
        University = 1,

        /// <summary>
        /// Работадатель
        /// </summary>
        [Descriptor("Работадатель")]
        Employer = 2,

        /// <summary>
        /// МОН РК
        /// </summary>

        [Descriptor("МОН РК")]
        Ministry = 3,

        /// <summary>
        /// Колледж
        /// </summary>
        [Descriptor("Колледж")]
        College = 4,

        /// <summary>
        /// Институт
        /// </summary>
        [Descriptor("Институт")]
        Institute = 5,

        /// <summary>
        /// Академия
        /// </summary>
        [Descriptor("Академия")]
        Academy = 6 

    }


    /// <summary>
    /// Типы специальностей
    /// <para>
    /// TYPE_SPEC = 1 - специальность
    /// </para>
    /// <para>
    /// TYPE_OP = 2 - Образовательная программа
    /// </para>
    /// </summary>
    public enum SpecialityType
    {
        /// <summary>
        /// Специальность
        /// </summary>
        TYPE_SPEC = 1,

        /// <summary>
        /// Образовательная программа
        /// </summary>
        TYPE_OP = 2
    }

    /// <summary>
    /// Типы дисциплины 
    /// </summary>
    /// <remarks>
    ///  NORMAL = 0 - Обычная 
    ///  ELECTIVE = 1 - Элективный 
    ///  PRACTICE = 2 - Практика 
    ///  SPECIAL = 3 - Спец. дисциплина
    /// </remarks> 
    public enum SubjectType
    {
        /// <summary>
        /// Обычная
        /// </summary>
        [Description("Обычная")]
        NORMAL = 0,

        /// <summary>
        /// Элективный
        /// </summary>
        [Description("Элективный")]
        ELECTIVE = 1,

        /// <summary>
        /// Практика
        /// </summary>
        [Description("Практика")]
        PRACTICE = 2,

        /// <summary>
        /// Спец. дисциплина
        /// </summary>
        [Description("Спец. дисциплина")]
        SPECIAL = 3
    }

    /// <summary>
    /// Swagger  фильтр для отбражения названии и значении  для Enum типов
    /// </summary>
    public class EnumSchemaFilter : ISchemaFilter
    {
        public void Apply(OpenApiSchema model, SchemaFilterContext context)
        {
            if (context.Type.IsEnum)
            {
                model.Enum.Clear();
                Enum.GetNames(context.Type)
                    .ToList()
                    .ForEach(name => model.Enum.Add(new OpenApiString($"{Convert.ToInt64(Enum.Parse(context.Type, name))} - {name}")));
            }
        }
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class DescriptorAttribute : Attribute
    {
        public string Description { get; }

        public DescriptorAttribute(string Description)
        {
           this.Description = Description;
        }
    }
    public static class EnumHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">Тип аттрибута</typeparam>
        /// <param name="enumVal">Значение перечисление</param>
        /// <returns>Объект тип аттрибута </returns>
        public static T GetAttributeOfType<T>(this Enum enumVal) where T : System.Attribute
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
            return (attributes.Length > 0) ? (T)attributes[0] : null;
        }
    }

}
