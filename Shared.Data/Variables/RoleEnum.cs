﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniverCert.Shared.Data.Context
{
        public enum RoleEnum
        {
            /// <summary>
            /// Не определен
            /// </summary>
            None = 0,

            /// <summary>
            /// Работадатель
            /// </summary>
            Verifier = 1,

            /// <summary>
            /// Университет
            /// </summary>
            Owner,

            /// <summary>
            /// МОН РК
            /// </summary>
            Head
    }
}
