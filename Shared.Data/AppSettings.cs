﻿namespace UniverCert.Shared.Data
{
    public class AppSettings
    {
        public AuthOption AuthOptions { get; set; }
        public MailSetting MailSettings { get; set; }
        public BlockChainSettings BlockChainSettings { get; set; } 
        public RabbitMqSettings RabbitMqSettings { get; set; } 

    }
    public class AuthOption
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string Key { get; set; }
        public int Lifetime { get; set; } = 0;
        public int LifetimeRefresh { get; set; } = 0;
    }
    public class MailSetting
    {
        public string From { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string SmtpServer { get; set; }
        public int Port { get; set; }
    }

    public class BlockChainSettings
    {
        public string ContractAddress { get; set; }
        public string OfChainServerUrl { get; set; }
        public string ABI { get; set; } 
    }

    public class RabbitMqSettings
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
    }
}
