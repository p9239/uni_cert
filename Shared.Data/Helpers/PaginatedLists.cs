﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniverCert.Shared.Data.Helpers
{
    public class PaginatedLists<T> 
    {
        public int PageIndex { get; private set; }
        public int TotalPages { get; private set; }
        public List<T> Items { get; private set; }
        public string Info { get; set; }

        public PaginatedLists(List<T> items, int count, int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            Items = items;
            Info = items == default || TotalPages < PageIndex ?
                "Данные не найдены" :
                $"Показано с {(pageIndex-1)*pageSize+1} по {(pageIndex != TotalPages ? (pageIndex-1)*pageSize + pageSize : count)} из {count} записей";
        }
        public bool HasPreviousPage
        {
            get
            {
                return PageIndex > 1;
            }
        }
        public bool HasNextPage
        {
            get
            {
                return PageIndex < TotalPages;
            }
        }

        public static async Task<PaginatedLists<T>> CreateAsync(IQueryable<T> source, int pageIndex, int pageSize)
        {
            if(pageIndex < 1)
                pageIndex = 1;
            var count = await source.CountAsync();
            var items = await source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToListAsync();
            return new PaginatedLists<T>(items, count, pageIndex, pageSize);
        }

    }
}
