﻿namespace UniverCert.Shared.Data.Context
{
    /// <summary>
    /// Тип контроля
    /// </summary>
    public class ControllType : BaseEntity<int>
    {/// <summary>
     /// Наименование на казахском языке
     /// </summary>
        public string NameKZ { get; set; }

        /// <summary>
        /// Наименование на русском языке
        /// </summary>
        public string NameRU { get; set; }

        /// <summary>
        /// Наименование на английском языке
        /// </summary>
        public string NameEN { get; set; }
    }
}
