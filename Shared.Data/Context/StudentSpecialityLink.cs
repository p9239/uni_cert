﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace UniverCert.Shared.Data.Context
{
    public class StudentSpecialityLink : BaseEntity<long>
    { 
        /// <summary>
        /// ИД студента в базе Блокчейн сети
        /// </summary>
        public long StudentId { get; set; }

        /// <summary>
        /// ИД специальности из каталога  Specialities в блокчейн сети
        /// </summary>
        public long  SpecialityId { get; set; }

        /*/// <summary>
        /// ИД вуза где проходил обучение данной специальности
        /// </summary>
        // TODO:  Тут берется SpecialityId з каталога базы блокчейна, поэтому на будущее надо убрать данное поле  и переделывать запросы в сервисах касаемое к HeiID
        public int HeiID { get; set; } 
        */ 
        [JsonIgnore]
        [ForeignKey(nameof(StudentId))]
        public Student Student { get; set; }

        [JsonIgnore]
        [ForeignKey(nameof(SpecialityId))]
        public Speciality Speciality { get; set; }

    }
}
