﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using UniverCert.Shared.Data.Variables;

namespace UniverCert.Shared.Data.Context
{
    /// <summary>
    /// Организации 
    /// </summary>
    public class Organization : BaseEntity<int>
    {
        /// <summary>
        /// Наименование на казахском языке
        /// </summary>
        public string NameKZ { get; set; }

        /// <summary>
        /// Наименование на русском языке
        /// </summary>
        public string NameRU { get; set; }

        /// <summary>
        /// Наименование на английском языке
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Адрес организации
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// БИН организации
        /// </summary>
        public string BIN { get; set; }

        /// <summary>
        /// Роль организации в блокчейн сети
        /// </summary> 
        public OrganizationRole Role { get; set; }

        [JsonIgnore]
        public ICollection<Speciality> Specialities { get; set; }

        /// <summary>
        /// Логин организации в бд Блокчейн
        /// </summary>
        public string BlockChainLogin{ get; set; }

        /// <summary>
        /// Шифрованный пароль организации в бд Блокчейн
        /// </summary>
        public string BlockChainPassword { get; set; }
        
        /// <summary>
        /// API организации для отправка результатов блокчейна
        /// </summary>
        public string BlockChainResultUri { get; set; }

        /// <summary>
        /// Секретный ключ для взаимодействие с API организации
        /// </summary>
        public string IntegrationSecret { get; set; }

    }
}
