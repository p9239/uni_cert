﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace UniverCert.Shared.Data.Context
{
    public class User : BaseEntity<Guid>
    {
        public string Login { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public RoleEnum Role { get; set; }
        public int PasswordTryCount { get; set; }
        public Boolean IsBlocked { get; set; }
        /// <summary>
        /// Идентификатор организации
        /// </summary>
        [ForeignKey(nameof(OrganizationId))]
        public Organization Organization { get; set; }
        public int OrganizationId { get; set; }
    }
}
