﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UniverCert.Shared.Data.Context
{
    /// <summary>
    /// Базовый класс
    /// </summary>
    public class BaseEntity<T>
    {
        /// <summary>
        /// Id
        /// </summary>  
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]  
        public T Id { get; set; }

        // <summary>
        /// Дата создания сущности
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Признак удаления
        /// </summary>
        public bool IsDeleted { get; set; } = false;
    }
}
