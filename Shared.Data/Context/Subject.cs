﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using UniverCert.Shared.Data.Variables;

namespace UniverCert.Shared.Data.Context
{
    /// <summary>
    /// Каталог дисциплин
    /// </summary>
    public class Subject : BaseEntity<long>
    {
        /// <summary>
        /// Наименование на казахском языке
        /// </summary>
        public string NameKZ { get; set; }

        /// <summary>
        /// Наименование на русском языке
        /// </summary>
        public string NameRU { get; set; }

        /// <summary>
        /// Наименование на английском языке
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// ИД дисциплины в LMS системе Вуза
        /// </summary>
        public int LmsSubjectId { get; set; }

        /// <summary>
        /// Код/РУП дисциплины на казахском языке
        /// </summary>
        public string CodeKZ { get; set; }

        /// <summary>
        /// Код/РУП дисциплины на русском языке
        /// </summary>
        public string CodeRU { get; set; }

        /// <summary>
        /// Код/РУП дисциплины на английском языке
        /// </summary>
        public string CodeEN { get; set; }

        /// <summary>
        /// Описание дисциплины на казахском языке
        /// </summary>
        public string DescKZ { get; set; }

        /// <summary>
        /// Описание дисциплины на русском языке
        /// </summary>
        public string DescRU { get; set; }

        /// <summary>
        /// Описание дисциплины на английском языке
        /// </summary>
        public string DescEN { get; set; }

        /// <summary>
        /// ИД Вуза/организации который относится данная дисциплина
        /// </summary>
        public int OrganizationId { get; set; }

        /// <summary>
        /// Тип дисциплины
        /// </summary> 
        [EnumDataType(typeof(SubjectType))]
        public SubjectType Type { get; set; }
    }
}
