﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using UniverCert.Shared.Data.Variables;

namespace UniverCert.Shared.Data.Context
{
    /// <summary>
    /// Специальности/образовательные программы
    /// </summary>
    public class Speciality : BaseEntity<long>
    { 
        /// <summary>
        /// Шифр специальности
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Наименование на казахском языке
        /// </summary>
        public string NameKZ { get; set; }

        /// <summary>
        /// Наименование на русском языке
        /// </summary>
        public string NameRU { get; set; }

        /// <summary>
        /// Наименование на английском языке
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Количество обязательных кредитов для освоения данной образовательной программы
        /// </summary>
        public int TotalCredit { get; set; }

        /// <summary>
        /// ИД специальности в LMS системе Вуза
        /// </summary>
        public int LmsSpecId { get; set; }
        
        /// <summary>
        /// ИД организации/Вуза данной специальности
        /// </summary>
        public int OrganizationId { get; set; }

        /// <summary>
        /// Квалификация при окончании данной специальности на казахском яызке
        /// </summary>
        public string QualificationKZ { get; set; }

        /// <summary>
        /// Квалификация при окончании данной специальности на русском яызке
        /// </summary>
        public string QualificationRU { get; set; }

        /// <summary>
        /// Квалификация при окончании данной специальности на английском яызке
        /// </summary>
        public string QualificationEN { get; set; }

        /// <summary>
        /// Тип специальности
        /// </summary>
        /// <remarks>
        ///  TYPE_SPEC = 1 - специальность
        ///  TYPE_OP = 2 - Образовательная программа
        /// </remarks>
        public SpecialityType Type { get; set; }


        /// <summary>
        /// Ступень обучения
        /// </summary>
        public int StageId { get; set; }

        [JsonIgnore]
        public ICollection<StudentSpecialityLink> SpecialityStudents{ get; set; } 

        [JsonIgnore]
        [ForeignKey(nameof(OrganizationId))]
        public Organization Organization { get; set; }


        [JsonIgnore]
        [ForeignKey(nameof(StageId))]
        public Stage Stage { get; set; }
    }
}
