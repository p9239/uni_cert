﻿using Microsoft.EntityFrameworkCore;

namespace UniverCert.Shared.Data.Context
{
    public class DataContext : DbContext
    {
        public DataContext() { }
        public DataContext(DbContextOptions<DataContext> options):base(options) { }
        public DbSet<User> Users { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Student> Students { get; set; } 
        public DbSet<Speciality> Specialities { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<StudentSpecialityLink> StudentSpecialityLink { get; set; }
        public DbSet<Stage> Stages { get; set; }
        public DbSet<EducationLevel> EducationLevels { get; set; }
        public DbSet<ControllType> ControllTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<StudentSpecialityLink>()
                .HasKey(t => new { t.Id }); 


            modelBuilder.Entity<Student>()
                .HasMany(e => e.Specialities)
                .WithOne(e => e.Student);

            modelBuilder.Entity<Speciality>()
                .HasMany(e => e.SpecialityStudents)
                .WithOne(e => e.Speciality);

            modelBuilder.Entity<Speciality>()
                .HasOne(e => e.Organization)
                .WithMany(x =>x.Specialities)
                .HasForeignKey(s => s.OrganizationId)
                .OnDelete(deleteBehavior: DeleteBehavior.NoAction);

            modelBuilder.Entity<Organization>()
                .HasMany(e => e.Specialities)
                .WithOne(e => e.Organization);

            modelBuilder.Entity<StudentSpecialityLink>()
                .HasOne(p => p.Student) 
                .WithMany(b => b.Specialities) 
                .HasForeignKey(s => s.StudentId) 
                .OnDelete(deleteBehavior: DeleteBehavior.Cascade);

            modelBuilder.Entity<StudentSpecialityLink>()
                .HasOne(p => p.Speciality)
                .WithMany(b => b.SpecialityStudents)
                .HasForeignKey(s => s.SpecialityId)
                .OnDelete(deleteBehavior: DeleteBehavior.NoAction);
        }
    }

}
