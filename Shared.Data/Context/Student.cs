﻿using System;
using System.Collections.Generic;

namespace UniverCert.Shared.Data.Context
{
    /// <summary>
    /// Данные студента
    /// </summary>
    public class Student : BaseEntity<long>
    {
        public Student()
        { 
            Specialities = new HashSet<StudentSpecialityLink>();
        }

        /// <summary>
        /// ИИН студента
        /// </summary>
        public string IIN { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string Sname { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string Fname { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime BirthDay { get; set; }

        /// <summary>
        /// Ид студента в LMS системе Вуза
        /// </summary>
        public int LmsStudentId { get; set; }

        /// <summary>
        /// Специальности студента
        /// </summary>
        public virtual ICollection<StudentSpecialityLink> Specialities { get; set; }
    }
 
}
