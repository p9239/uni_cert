﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UniverCert.Shared.Models;

namespace UniverCert.Shared.Services
{
    public interface IMailService
    {
        /// <summary>
        /// Отправка сообщение на почту
        /// </summary>
        /// <param name="message"></param>
        /// <returns>Успешность этого операции</returns>
        Task<bool> SendEmailAsync(MessageMail message);
        /// <summary>
        /// Отправка список сообщении
        /// </summary>
        /// <param name="messages"></param>
        /// <returns>Количество отправленных сообщении</returns>
        Task<int> SendEmailsAsync(params MessageMail[] messages);
    }
}
