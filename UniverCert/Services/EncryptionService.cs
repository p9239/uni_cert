﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace UniverCert.Shared.Services
{
    public class EncryptionService : IEncryptionService
    {
        public string Encrypt(string plainText, string key)
        {
            if (plainText == null /*|| plainText.Length <= 0*/)
                throw new ArgumentNullException("plainText");
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("Key");

            using (Aes aes = Aes.Create())
            {
                aes.BlockSize = 128;
                aes.KeySize = 128;
                aes.Key = Encoding.UTF8.GetBytes(GetSignedKey16(key));
                aes.IV = Encoding.UTF8.GetBytes(GenerateSignedIV16(key));

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        } 
                        return Convert.ToBase64String(msEncrypt.ToArray());
                    }
                }
            }           
        }
        public string Decrypt(string encryptedText, string key)
        {
            if (encryptedText == null || encryptedText.Length <= 0)
                throw new ArgumentNullException("encryptedText");
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("Key"); 

            using (Aes aes = Aes.Create())
            {
                aes.BlockSize = 128;
                aes.KeySize = 128;
                aes.Key = Encoding.UTF8.GetBytes(GetSignedKey16(key));
                aes.IV = Encoding.UTF8.GetBytes(GenerateSignedIV16(key));

                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                byte[] cipherText = Convert.FromBase64String(encryptedText);

                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            return srDecrypt.ReadToEnd(); 
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Получения 16 значный ключ
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetSignedKey16(string key)
        { 
            string k = "&*^%HG*&0sdKI*Dsd65f$E#@";
            int l = key.Length;
            char[] chars = new char[16];
            for (int i = 0; i < 16; i++)
            {
                if (i < l)
                    chars[i] = key[i];
                else
                    chars[i] = k[i];
            }
            return new String(chars);
        }
        /// <summary>
        /// Получения 16 значный IV
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GenerateSignedIV16(string key)
        {
            string k = "$g^1#h(^&h74_d0hfUhg&8";
            int l = key.Length;
            char[] chars = new char[16];
            for (int i = 0; i < 16; i++)
            {
                if (i < l)
                    chars[i] = key[l - i - 1];
                else
                    chars[i] = k[i];
            }
            return new String(chars);
        }
    }
}
