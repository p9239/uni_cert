﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniverCert.Shared.Services
{
    public interface IEncryptionService
    {
        /// <summary>
        /// Шифрование текста по ключу
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="key"></param>
        /// <returns>Шифрованный текст</returns>
       string Encrypt(string plainText, string key);

        /// <summary>
        /// Расшифрование текста по ключу
        /// </summary>
        /// <param name="encryptedText"></param>
        /// <param name="key"></param>
        /// <returns>Расшифрованный текст</returns>
        string Decrypt(string encryptedText, string key);
    }
}
