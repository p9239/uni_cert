﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UniverCert.Shared.Data;
using UniverCert.Shared.Models;

namespace UniverCert.Shared.Services
{
    public class MailService : IMailService
    {
        private readonly IOptions<AppSettings> _options;
        public MailService(IOptions<AppSettings> options)
        {
            _options = options;
        }
        public async Task<bool> SendEmailAsync(MessageMail message)
        {
            try
            {
                if(message.ToEmail == default)
                    return false;

                MimeMessage email = new MimeMessage();
                email.Sender = MailboxAddress.Parse(_options.Value.MailSettings.From);
                email.To.Add(MailboxAddress.Parse(message.ToEmail));
                email.Subject = message.Subject;
                var builder = new BodyBuilder();
                builder.HtmlBody = message.Body;
                email.Body = builder.ToMessageBody();

                using(var smtp = new SmtpClient())
                {
                    smtp.Connect(_options.Value.MailSettings.SmtpServer, _options.Value.MailSettings.Port, SecureSocketOptions.StartTls);
                    smtp.AuthenticationMechanisms.Remove("XOAUTH2");
                    smtp.Authenticate(_options.Value.MailSettings.Username, _options.Value.MailSettings.Password);
                    await smtp.SendAsync(email);
                };
                return true;
            }
            catch (Exception ex)
            {
                Log.Error($"Send email exception: {ex.Message}");
                return false;
            }
        }

        public async Task<int> SendEmailsAsync(params MessageMail[] messages)
        {
            int sentCount = 0;
            using var smtp = new SmtpClient();
            try
            {
                smtp.Connect(_options.Value.MailSettings.SmtpServer, _options.Value.MailSettings.Port, SecureSocketOptions.StartTls);
                smtp.AuthenticationMechanisms.Remove("XOAUTH2");
                smtp.Authenticate(_options.Value.MailSettings.Username, _options.Value.MailSettings.Password);

                foreach (var message in messages)
                {
                    if (message.ToEmail == default)
                        continue;

                    MimeMessage email = new MimeMessage();
                    email.Sender = MailboxAddress.Parse(_options.Value.MailSettings.From);
                    email.To.Add(MailboxAddress.Parse(message.ToEmail));
                    email.Subject = message.Subject;
                    var builder = new BodyBuilder();
                    builder.HtmlBody = message.Body;
                    email.Body = builder.ToMessageBody();

                    await smtp.SendAsync(email);
                    sentCount++;
                }   
            }
            catch (Exception ex)
            {
                Log.Error($"Send emails exception: {ex.Message}");
            }
            finally
            {  
                smtp.Disconnect(true);
                smtp.Dispose();
            }

            return sentCount;
        }
    }
}
