﻿using Autofac;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniverCert.Shared.Data;
using UniverCert.Shared.Data.Context;
using UniverCert.Shared.Data.Variables;

namespace UniverCert
{
    /// <summary>
    /// Базовый startup
    /// </summary>
    public class StartupShared
    {
        private readonly IConfiguration _configuration; 
        public StartupShared(IConfiguration configuration, IHostEnvironment env)
        { 
            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder
                .AddJsonFile($"appsettings.shared.json") 
                .AddConfiguration(configuration);
            Configuration = configurationBuilder.Build();
            if (Configuration["Serilog:WriteTo:1:Args:path"] != null)
                Configuration["Serilog:WriteTo:1:Args:path"] =
                    Configuration["Serilog:WriteTo:1:Args:path"].Replace("{Application}", env.ApplicationName);
            Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(Configuration)
            .WriteTo.Seq("http://localhost:5341")
            .CreateLogger();
        }
        protected IConfiguration Configuration { get; }
        protected IHostEnvironment Environment { get; set; }

        protected void ConfigureContainer(ContainerBuilder builder)
        {          
        }

        protected void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")).EnableSensitiveDataLogging(), ServiceLifetime.Scoped);
            //var ct = Configuration.GetConnectionString("DefaultConnection");
            services.Configure<AppSettings>(Configuration.GetSection(nameof(AppSettings)));

            services.AddCors();
            services.AddControllers()
               .AddNewtonsoftJson(opt =>
                   opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddOptions();
            services.AddHttpContextAccessor();
            services.Configure<RouteOptions>(options => options.LowercaseUrls = true);
            services.AddSwaggerGen(c =>
            {
                 #if (DEBUG)
                    c.IncludeXmlComments(string.Format(@"{0}\ApiDocumentation.xml", System.AppDomain.CurrentDomain.BaseDirectory));
                    c.SwaggerDoc("v1", new OpenApiInfo 
                    { 
                        Title = "UniverCert App with .Net Core v3.1", 
                        Version = "v1" 
                    });
                #endif
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization. Введите токен в заголовок. Пример: \"Bearer {token}\"",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    In = ParameterLocation.Header,
                    Scheme = "Bearer"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Type = SecuritySchemeType.ApiKey,
                            Name = "Bearer",
                            In = ParameterLocation.Header
                        },
                        new string[]{}
                    }
                });
                c.SchemaFilter<EnumSchemaFilter>();
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.RequireHttpsMetadata = false;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidIssuer =  Configuration["AppSettings:AuthOptions:Issuer"],
                            ValidateAudience = true,
                            ValidAudience = Configuration["AppSettings:AuthOptions:Audience"],
                            ValidateLifetime = true,
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["AppSettings:AuthOptions:Key"])),
                            ValidateIssuerSigningKey = true,
                            ClockSkew = TimeSpan.Zero
                        };

                        options.Events = new JwtBearerEvents
                        {
                            OnAuthenticationFailed = context =>
                            {
                                if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                                {
                                    context.Response.Headers.Add("X-Token-Expired", "true");
                                    context.Response.Headers.Add("Access-Control-Expose-Headers", "X-Token-Expired");
                                }

                                return Task.CompletedTask;
                            }
                        };
                    });
            services.AddControllersWithViews();
            services.AddResponseCompression();

        }

        protected void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory logger)
        {
            app.UseResponseCompression();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader());
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.None);
                string swaggerJsonBasePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";
                c.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/v1/swagger.json", "UniverCert Api");
            });
            app.UseRouting();
            app.UseAuthentication();
            logger.AddSerilog();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
