﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using System;
using System.Linq;
using System.Security.Claims;
using UniverCert.Shared.Data.Context;

namespace UniverCert.Shared.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class BaseController : ControllerBase
    {
        private Guid _userId = new Guid();
        private RoleEnum _userRole = RoleEnum.None;
        private (string Login, string Password, string Uri, string Secret) _blockChainData = default;
        private int _userOrganizationId ;

        protected int CurrentUserOrganizationId
        {
            get
            {
                if (_userOrganizationId != default) return _userOrganizationId;
                
                var claimValue = User
                    .FindFirst(x => x.Type == "organizationId")?
                    .Value;
                if (claimValue == null) return _userOrganizationId;
                _userOrganizationId = int.Parse(claimValue);

                return _userOrganizationId;
            }
        }
        protected Guid CurrentUserId
        {
            get
            {
                if (_userId != default) return _userId;
                var claimValue = User
                    .Claims
                    .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
                if (claimValue == null) return _userId;
                _userId = Guid.Parse(claimValue);

                return _userId;
            }
        }

        protected RoleEnum CurrentUserRole
        {
            get
            {
                if (_userRole != RoleEnum.None) return _userRole;
                var claimValue = User
                    .Claims
                    .FirstOrDefault(x => x.Type == ClaimsIdentity.DefaultRoleClaimType)?.Value;
                if (claimValue == null) return RoleEnum.None;
                _userRole = (RoleEnum)Enum.Parse(typeof(RoleEnum), claimValue);

                return _userRole;
            }
        }
 
        protected (string Login, string Password, string Uri,string Secret) BlockChainAccount
        {
            get
            {
                if (_blockChainData.Login != default || _blockChainData.Password != default) return _blockChainData;

                _blockChainData.Login = User.FindFirst(x => x.Type == "orgAccount")?.Value;
                _blockChainData.Password = User.FindFirst(x => x.Type == "orgPass")?.Value;
                _blockChainData.Uri = User.FindFirst(x => x.Type == "orgUri")?.Value;
                _blockChainData.Secret = User.FindFirst(x => x.Type == "orgSecret")?.Value;

                return _blockChainData;
            }
        }


        /// <summary>
        /// Логирование ошибок
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="args"></param>
        /// <returns>StatusCode</returns>
        protected IActionResult ExceptionResult(Exception ex, object args = null)
        {
            var controllerName = ControllerContext.ActionDescriptor.ControllerName;
            var actionName = ControllerContext.ActionDescriptor.ActionName;
#if DEBUG
            var msg = $"{controllerName} {actionName} {ex.Message}";
#else
            var msg = $"{ex.Message}";
#endif
            var detailMsg = $"{ex.Message}, Trace: {ex.StackTrace}";
            switch (ex)
            {
                case Exception e when e is ArgumentException:
                    //Log.Warning(ex, msg, args);
                    return BadRequest(msg);

                case Exception e when e is UnauthorizedAccessException:
                    Log.Warning(ex, msg, args);
                    return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
#if DEBUG
                default:
                    Log.Error(ex, msg, args);
                    return StatusCode(StatusCodes.Status500InternalServerError, detailMsg);
#else
                default:
                    Log.Error(ex, msg, args);
                    return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
#endif
            }
        }
    }
}
