﻿using Microsoft.AspNetCore.Authorization;
using System.Linq;
using UniverCert.Shared.Data.Context;

namespace UniverCert.Shared
{
    public class UserRoleAttribute : AuthorizeAttribute
    {
        public UserRoleAttribute(params RoleEnum[] roles)
        {
            Roles = string.Join(",", roles.Select(x => x.ToString()));
        }
    }
}
