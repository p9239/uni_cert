﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UniverCert.Shared.Models
{
    public class MessageMail
    {
        public readonly string ToEmail;

        public readonly string Subject = "Сообщение от блокчейн проекта";

        public readonly string Body = "Пустое сообщение";

        public MessageMail(string ToEmail,string Subject, string Body)
        {
            this.ToEmail = ToEmail;
            if(!String.IsNullOrWhiteSpace(Subject))
                this.Subject = Subject;
            if (!String.IsNullOrWhiteSpace(Body))
                this.Body = Body;
        }
    }
}
