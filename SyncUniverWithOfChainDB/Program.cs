﻿using Models;
using SyncUniverWithOfChainDB.API;
using SyncUniverWithOfChainDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncUniverWithOfChainDB
{
    class Program
    {
        private static string AUTH_PATH { get { return ConfigurationModel.getAppValue("authUrl");  } }
        private static string OFCHAIN_API_URL { get { return ConfigurationModel.getAppValue("OfChainUrl"); } }
        private static string SPECIALITY_PUT_LIST_PATH { get { return OFCHAIN_API_URL + ConfigurationModel.getAppValue("specialityPutListPath"); } }


        public static void Main(string[] args)
        {
            Api.logPath = ConfigurationModel.getAppValue("logPath");
            string reqResult = Api.AuthRequest(AUTH_PATH);
            try
            {
                Api.TOKEN = Api.deserialize<AccessTokenModel>(reqResult).AccessToken;

                #region Отправка специальностей
                List<SpecialityModel> spList = SpecialityModel.GetList();
                int sendCount = 5;
                StringBuilder jsonValues = new StringBuilder();
                int pages = (spList.Count / sendCount) + (spList.Count % sendCount != 0 ? 1 : 0);
                while (pages > 0)
                {
                    jsonValues.Clear();
                    for (int i = 0; i < Math.Min(sendCount, spList.Count); i++)
                    {
                        jsonValues.Append("," + Api.serialize(spList.ElementAt(i)));
                    }
                    spList.RemoveRange(0, Math.Min(sendCount, spList.Count));
                    string json = "[" + jsonValues.Remove(0, 1).ToString() + "]";
                    try
                    {
                        string res =  Api.doRequest(SPECIALITY_PUT_LIST_PATH, json);
                        Api.logWrite(Environment.NewLine + "Отправлено список специальности, json:\n" + json + "\nОтвет:\n" + res);
                        Console.WriteLine("Sended: " + json + Environment.NewLine);
                    }
                    catch (Exception ex)
                    { 
                        Api.logWrite(Environment.NewLine + "Ошибка при отправке " + ex.Message + ", json:\n" + json);
                        Console.WriteLine(Environment.NewLine + "Ошибка при отправке " + ex.Message + ", json:\n" + json);
                        break;
                    }
                    pages--;
                }
                #endregion
            }
            catch (Exception e)
            {
                Console.WriteLine(reqResult);
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
           
        }
    }
}
