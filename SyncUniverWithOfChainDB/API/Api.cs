﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;

namespace SyncUniverWithOfChainDB.API
{
    public class Api
    {
        public static string authority { get; set; }

        public static string logPath;

        public static string TOKEN { get; set; }



        public static void logWrite(string log)
        {
            DateTime now = DateTime.Now;
            string fullPath = logPath + "/" + now.Year + "/" + now.ToString("MMMM").ToLower() + "/";
            if (!Directory.Exists(fullPath)) Directory.CreateDirectory(fullPath);
            string fullFileName = fullPath + now.Day + ".log";
            if (!File.Exists(fullFileName)) (File.Create(fullFileName)).Close();
            System.IO.File.AppendAllText(fullFileName, now.ToString() + "\n" + log + "\n\n");
        }

        public static void errorWrite(string log)
        {
            Console.WriteLine(log);
            DateTime now = DateTime.Now;
            string fullPath = logPath + "/" + now.Year + "/" + now.ToString("MMMM").ToLower() + "/";
            if (!Directory.Exists(fullPath)) Directory.CreateDirectory(fullPath);
            string fullFileName = fullPath + now.Day + "_error.log";
            if (!File.Exists(fullFileName)) (File.Create(fullFileName)).Close();
            System.IO.File.AppendAllText(fullFileName, now.ToString() + "\n" + log);
            Console.WriteLine("ErrorWriteSuccess");
        }

        public static string pathCombine(string path1, string path2)
        {
            int ind1 = path1.Length - 1;
            if (path1[ind1] != '/' && path1[ind1] != '\\') path1 += '/';
            if (path2[0] == '/' || path2[0] == '\\') path2 = path2.Remove(0, 1);
            return path1 + path2;
        }


       /* public static void allOperationsPut<T>(SqlConnection con, string putUrl, Func<SqlConnection, int, List<T>> getTopList, Action<SqlConnection, List<T>> afterPutting, int top = 50)
        {
            int total = 0;
            string modelName = typeof(T).Name.ToLower();
            while (true)
            {
                List<T> list = getTopList(con, top);
                if (list.Count == 0) break;
                string json = serialize(list);
                try
                {
                    string res = doRequest(putUrl, json);
                    logWrite("Данные отправлены, модель: " + modelName + ", количество: " + list.Count + " общее" + total + ", json:\n" + json + "\nОтвет:\n" + res); 
                    afterPutting(con, list);
                    total += list.Count;
                }
                catch (Exception ex)
                { 
                    logWrite("Ошибка при отправке, модель: " + modelName + ", количество: " + list.Count + ", json:\n" + json);
                    errorWrite("Ошибка при отправке, модель: " + modelName + ", ошибка: " + ex.ToString());
                    break;
                }
            }
            Api.logWrite("Модель: " + modelName + ", всего отправлено: " + total); 
        }*/

        public static string serialize(Object o)
        {
            JavaScriptSerializer ser = new  JavaScriptSerializer();
            ser.MaxJsonLength = int.MaxValue;
            return ser.Serialize(o);
        }

        public static T deserialize<T>(string json)
        {
             JavaScriptSerializer ser = new  JavaScriptSerializer();
            ser.MaxJsonLength = Int32.MaxValue;
            return ser.Deserialize<T>(json);
        }

        public static string doRequest( string url, string jsonValues)
        { 
            var http = (HttpWebRequest)WebRequest.Create(new Uri(url));
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";
            http.Headers.Add("Bearer", value: TOKEN);
             
            Byte[] bytes = System.Text.Encoding.UTF8.GetBytes(jsonValues);
            Stream newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();
            var response = http.GetResponse();
            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();
            return content;
        }

        public static string AuthRequest(string authUrl)   
        { 
            var http = (HttpWebRequest)WebRequest.Create(new Uri(authUrl));
            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = "POST";

            AuthModel authModel = new AuthModel(ConfigurationModel.getAppValue("login"), ConfigurationModel.getAppValue("password"));
            string parsedContent = serialize(authModel); // "{\"login\": \"" + ConfigurationModel.getAppValue("login") + "\", \"password\":\"" + ConfigurationModel.getAppValue("password") + "\"" + "}";
            Byte[] bytes = System.Text.Encoding.UTF8.GetBytes(parsedContent);
            Stream newStream = http.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();
            var response = http.GetResponse();
            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();
            return content;
        }
    }
}
