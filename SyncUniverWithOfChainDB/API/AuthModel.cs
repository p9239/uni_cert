﻿namespace SyncUniverWithOfChainDB.API
{
    public class AuthModel
    {
        public AuthModel(string _login, string _password)
        {
            login = _login;
            password = _password;
        }
        public string login { get; private set; }
        public string password { get; private set; } 
    }

    public class AccessTokenModel
    {
        public string AccessToken { get; set; } 
    }
}
