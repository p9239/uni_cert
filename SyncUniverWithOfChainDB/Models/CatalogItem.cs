﻿using System;

namespace SyncUniverWithOfChainDB.Models
{ /// <summary>
  /// Базовый класс
  /// </summary>
    public class CatalogItem<T>
    {
        /// <summary>
        /// Id
        /// </summary>   
        public T Id { get; set; }

        // <summary>
        /// Дата создания сущности
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Признак удаления
        /// </summary>
        public bool IsDeleted { get; set; } = false;
    }
}
