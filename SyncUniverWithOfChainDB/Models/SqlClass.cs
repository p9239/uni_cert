﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

/// <summary>Класс-помошник, для работы с MSSQL сервером на базе SQLClient</summary>
public class SqlClass
{
    public const int TRANSACT_COMMIT = 1;
    public const int TRANSACT_ROLLBACK = 2;

    /// <summary>ConnectionString для соединения с базой данных</summary>
    public string connectionString;

    /// <summary>Список параметров</summary>
    private ArrayList paramsList;

    /// <summary>Конструктор</summary><param name="conStr">ConnectionString</param>
    public SqlClass(String conStr)
    {
        connectionString = conStr;
        paramsList = new ArrayList();
    }

    /// <summary>Выполняем запрос к базе данных с параметрами</summary><param name="query">Текст запроса</param><param name="paramsList">Список параметров</param>
    public int query(string queryText)
    {
        return query(queryText, null, 0, 0);
    }

    /// <summary>Выполняем запрос к базе данных используя транзакции</summary><param name="queryText">Текст запроса</param><param name="transactionName">Имя транзакции (null - запрос без транзацкии)</param><param name="transactionResult">Результат транзации: Commit: SqlClass.TRANSACT_COMMIT, Rollback: SqlClass.TRANSACT_ROLLBACK</param><param name="isolationLevel">уровень изоляции</param><param name="paramsList">Список параметров</param>
    public int query(string queryText, string transactionName, int transactionResult, IsolationLevel isolationLevel)
    {
        SqlConnection myConnection = new SqlConnection(connectionString);
        myConnection.Open();
        SqlCommand command = myConnection.CreateCommand();
        if (transactionName != null && transactionResult > 0)
            command.Transaction = myConnection.BeginTransaction(isolationLevel, transactionName); ;

        command.CommandText = queryText;
        fillCommandWithParams(command, paramsList);
        int ret = -1;
        try
        {
            ret = command.ExecuteNonQuery();
            if (transactionName != null && transactionResult > 0)
                switch (transactionResult)
                {
                    case SqlClass.TRANSACT_COMMIT: command.Transaction.Commit(); break;
                    case SqlClass.TRANSACT_ROLLBACK: command.Transaction.Rollback(); break;
                    default: command.Transaction.Commit(); break;
                }
        }
        catch (Exception e)
        {
            if (transactionName != null && transactionResult > 0)
                command.Transaction.Rollback();
            throw e;
        }
        finally
        {
            myConnection.Close();
        }

        return ret;
    }
     
    /// <summary>Выполняем запрос типа INSERT с параметрами</summary><param name="query">Текст запроса</param><param name="paramsList">Список параметров</param>
    public int insert(String query)
    {
        return insert(query, null, 0, 0);
    }

    /// <summary>Выполняем запрос типа INSERT с параметрами используя транзакции</summary><param name="queryText">Текст запроса</param><param name="transactionName">Имя транзакции (null - запрос без транзацкии)</param><param name="transactionResult">Результат транзации: Commit: MSSQLClass.TRANSACT_COMMIT, Rollback: MSSQLClass.TRANSACT_ROLLBACK</param><param name="isolationLevel">уровень изоляции</param><param name="paramsList">Список параметров</param>
    public int insert(string queryText, string transactionName, int transactionResult, IsolationLevel isolationLevel)
    {
        SqlConnection myConnection = new SqlConnection(connectionString);
        myConnection.Open();
        SqlCommand command = myConnection.CreateCommand();
        if (transactionName != null && transactionResult > 0)
            command.Transaction = myConnection.BeginTransaction(isolationLevel, transactionName); ;

        command.CommandText = queryText;
        fillCommandWithParams(command, paramsList);
        try
        {
            command.ExecuteNonQuery();
            if (transactionName != null && transactionResult > 0)
                switch (transactionResult)
                {
                    case SqlClass.TRANSACT_COMMIT: command.Transaction.Commit(); break;
                    case SqlClass.TRANSACT_ROLLBACK: command.Transaction.Rollback(); break;
                    default: command.Transaction.Commit(); break;
                }
        }
        catch (Exception e)
        {
            if (transactionName != null && transactionResult > 0)
                command.Transaction.Rollback();
            throw e;
        }
        object ret = null;

        try
        {
            command = myConnection.CreateCommand();
            command.CommandText = "SELECT @@IDENTITY";
            ret = command.ExecuteScalar();
            myConnection.Close();
            if (ret != DBNull.Value)
                return Convert.ToInt32(ret);
            else
                return -1;
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            myConnection.Close();
        }
    }

    /// <summary>Выполняем запрос на выбор с параметрами</summary><param name="query">Текст запроса</param><param name="paramsList">Список параметров</param>
    public SqlDataReader select(String query)
    {
        return select(query, null, 0);
    }

    /// <summary>Выполняем запрос на выбор с параметрами используя транзакции</summary><param name="queryText">Текст запроса</param><param name="transactionName">Имя транзакции (null - запрос без транзацкии)</param><param name="isolationLevel">уровень изоляции</param><param name="paramsList">Список параметров</param>
    public SqlDataReader select(string queryText, string transactionName, IsolationLevel isolationLevel)
    {
        SqlConnection myConnection = new SqlConnection(connectionString);
        myConnection.Open();
        SqlCommand command = myConnection.CreateCommand();
        if (transactionName != null)
            command.Transaction = myConnection.BeginTransaction(isolationLevel, transactionName);

        command.CommandText = queryText;
        fillCommandWithParams(command, paramsList);
        command.CommandTimeout = 1500;
        SqlDataReader ret = null;
        ret = command.ExecuteReader(CommandBehavior.CloseConnection);
        return ret;
    }

    /// <summary>Получаем значение из первой строки и первого столбца, полученных в результате запроса к БД query с параметрами</summary><param name="query">Текст запроса</param><param name="paramsList">Список параметров</param>
    public object qresult(String query)
    {
        return qresult(query, null, 0, 0);
    }

    /// <summary>Получаем значение из первой строки и первого столбца, полученных в результате запроса к БД query с параметрами используя транзакции</summary><param name="queryText">Текст запроса</param><param name="transactionName">Имя транзакции (null - запрос без транзацкии)</param><param name="transactionResult">Результат транзации: Commit: MSSQLClass.TRANSACT_COMMIT, Rollback: MSSQLClass.TRANSACT_ROLLBACK</param><param name="isolationLevel">уровень изоляции</param><param name="paramsList">Список параметров</param>
    public object qresult(string queryText, string transactionName, int transactionResult, IsolationLevel isolationLevel)
    {
        SqlConnection myConnection = new SqlConnection(connectionString);
        myConnection.Open();
        SqlCommand command = myConnection.CreateCommand();
        if (transactionName != null && transactionResult > 0)
            command.Transaction = myConnection.BeginTransaction(isolationLevel, transactionName); ;

        command.CommandText = queryText;
        fillCommandWithParams(command, paramsList);
        object ret = null;
        try
        {
            ret = command.ExecuteScalar();
            if (transactionName != null && transactionResult > 0)
                switch (transactionResult)
                {
                    case SqlClass.TRANSACT_COMMIT: command.Transaction.Commit(); break;
                    case SqlClass.TRANSACT_ROLLBACK: command.Transaction.Rollback(); break;
                    default: command.Transaction.Commit(); break;
                }
        }
        catch (Exception e)
        {
            if (transactionName != null && transactionResult > 0)
                command.Transaction.Rollback();
            throw e;
        }
        finally
        {
            myConnection.Close();
        }

        return ret;
    }
     

    /// <summary>Добавляем параметр</summary><param name="name">Имя</param><param name="type">Тип</param><param name="length">Длина</param><param name="value">Значение</param>
    public void addParam(string name, SqlDbType type, int length, object value)
    {
        paramsList.Add(new MyParameter(name, type, length, value));
    }

    /// <summary>Очищаем список параметров</summary>
    public void clearParams()
    {
        paramsList.Clear();
    }

    /// <summary>Вспомагательная функция, заполняющая указанную каманду параметрами</summary><param name="cmd">Камманда</param><param name="queryParams">Список параметров</param>
    private void fillCommandWithParams(SqlCommand cmd, ArrayList queryParams)
    {
        foreach (MyParameter p in queryParams)
        {
            if (p.parameterValue == null)
                cmd.Parameters.Add(p.parameterName, p.parameterType, p.parameterSize).Value = DBNull.Value;
            else
                cmd.Parameters.Add(p.parameterName, p.parameterType, p.parameterSize).Value = p.parameterValue;
        }
    }
     
}

/// <summary>Класс хранения параметров</summary>
public class MyParameter
{
    /// <summary>Название параметра (имя переменной)</summary>
    public String parameterName;
    /// <summary>Значение параметра</summary>
    public object parameterValue;
    /// <summary>Тип параметра</summary>
    public SqlDbType parameterType;
    /// <summary>Длина значения</summary>
    public int parameterSize;

    public MyParameter(String name, SqlDbType type, int size, object value)
    {
        parameterName = name;
        parameterType = type;
        parameterValue = value;
        parameterSize = size;
    }
}
 