﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncUniverWithOfChainDB.Models
{
    public class SubjectModel
    {
        /// <summary>
        /// Наименование на казахском языке
        /// </summary>
        public string NameKZ { get; set; }

        /// <summary>
        /// Наименование на русском языке
        /// </summary>
        public string NameRU { get; set; }

        /// <summary>
        /// Наименование на английском языке
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// ИД дисциплины в LMS системе Вуза
        /// </summary>
        public int LmsSubjectId { get; set; }

        /// <summary>
        /// Код/РУП дисциплины на казахском языке
        /// </summary>
        public string CodeKZ { get; set; }

        /// <summary>
        /// Код/РУП дисциплины на русском языке
        /// </summary>
        public string CodeRU { get; set; }

        /// <summary>
        /// Код/РУП дисциплины на английском языке
        /// </summary>
        public string CodeEN { get; set; }

        /// <summary>
        /// Описание дисциплины на казахском языке
        /// </summary>
        public string DescKZ { get; set; }

        /// <summary>
        /// Описание дисциплины на русском языке
        /// </summary>
        public string DescRU { get; set; }

        /// <summary>
        /// Описание дисциплины на английском языке
        /// </summary>
        public string DescEN { get; set; }

        /// <summary>
        /// ИД Вуза/организации который относится данная дисциплина
        /// </summary>
        public int OrganizationId { get; set; }

        /// <summary>
        /// Тип дисциплины
        /// </summary> 
        [EnumDataType(typeof(SubjectType))]
        public SubjectType Type { get; set; }
    }

    /// <summary>
    /// Типы дисциплины 
    /// </summary>
    /// <remarks>
    ///  NORMAL = 0 - Обычная 
    ///  ELECTIVE = 1 - Элективный 
    ///  PRACTICE = 2 - Практика 
    ///  SPECIAL = 3 - Спец. дисциплина
    /// </remarks> 
    public enum SubjectType
    {
        /// <summary>
        /// Обычная
        /// </summary>
        [Description("Обычная")]
        NORMAL = 0,

        /// <summary>
        /// Элективный
        /// </summary>
        [Description("Элективный")]
        ELECTIVE = 1,

        /// <summary>
        /// Практика
        /// </summary>
        [Description("Практика")]
        PRACTICE = 2,

        /// <summary>
        /// Спец. дисциплина
        /// </summary>
        [Description("Спец. дисциплина")]
        SPECIAL = 3
    }
}
