﻿namespace Models
{
    /// <summary>Класс для работы с конфигурационными данными (app.config)</summary>
    public class ConfigurationModel
    { 
        /// <summary>Ключ параметра конфигурации</summary>
        public string key;           

        /// <summary>Чтение значения из корневого файла конфигурации app.config из секции "configuration/appSettings/"</summary><param name="paramName">Название параметра конфигурации</param>
        public static string getConnectionString(string paramName)
        {
            string sTemp = System.Configuration.ConfigurationManager.ConnectionStrings[paramName].ConnectionString;
            if (sTemp == null)
                return null;
            else
                return sTemp;
        }

        public static string getAppValue(string key)
        {
            string sTemp = System.Configuration.ConfigurationManager.AppSettings[key];
            if (sTemp == null)
                return null;
            else
                return sTemp;
        } 

    }
}
