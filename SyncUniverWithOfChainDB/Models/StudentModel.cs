﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
namespace SyncUniverWithOfChainDB.Models
{
    public class StudentModel
    {
        public StudentModel()
        {
            Specialities = new HashSet<StudentSpecialityLink>();
        }

        /// <summary>
        /// ИИН студента
        /// </summary>
        public string IIN { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string Sname { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string Fname { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime BirthDay { get; set; }

        /// <summary>
        /// Ид студента в LMS системе Вуза
        /// </summary>
        public int LmsStudentId { get; set; }

        /// <summary>
        /// Специальности студента
        /// </summary>
        public virtual ICollection<StudentSpecialityLink> Specialities { get; set; } 
          
    }

    public class StudentSpecialityLink
    {
        /// <summary>
        /// ИД записи в таблице
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// ИД студента в базе Блокчейн сети
        /// </summary>
        public long StudentId { get; set; }

        /// <summary>
        /// ИД специальности из каталога  Specialities в блокчейн сети
        /// </summary>
        public long SpecialityId { get; set; }

        /// <summary>
        /// ИД вуза где проходил обучение данной специальности
        /// </summary>
        // TODO:  Тут берется SpecialityId з каталога базы блокчейна, поэтому на будущее надо убрать данное поле  и переделывать запросы в сервисах касаемое к HeiID
        public int HeiID { get; set; } 

    }
}
