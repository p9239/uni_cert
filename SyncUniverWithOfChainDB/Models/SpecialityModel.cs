﻿using Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SyncUniverWithOfChainDB.Models
{
    public class SpecialityModel : CatalogItem<int>
    {
        /// <summary>
        /// Шифр специальности
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Наименование на казахском языке
        /// </summary>
        public string NameKZ { get; set; }

        /// <summary>
        /// Наименование на русском языке
        /// </summary>
        public string NameRU { get; set; }

        /// <summary>
        /// Наименование на английском языке
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Количество обязательных кредитов для освоения данной образовательной программы
        /// </summary>
        public int TotalCredit { get; set; }

        /// <summary>
        /// ИД специальности в LMS системе Вуза
        /// </summary>
        public int LmsSpecId { get; set; } 

        /// <summary>
        /// Квалификация при окончании данной специальности на казахском яызке
        /// </summary>
        public string QualificationKZ { get; set; }

        /// <summary>
        /// Квалификация при окончании данной специальности на русском яызке
        /// </summary>
        public string QualificationRU { get; set; }

        /// <summary>
        /// Квалификация при окончании данной специальности на английском яызке
        /// </summary>
        public string QualificationEN { get; set; }

        /// <summary>
        /// Тип специальности
        /// </summary>
        /// <remarks>
        ///  TYPE_SPEC = 1 - специальность
        ///  TYPE_OP = 2 - Образовательная программа
        /// </remarks>
        public int Type { get; set; }

        /// <summary>
        /// Ступень обучения
        /// </summary>
        public int StageId { get; set; }

        public static List<SpecialityModel> GetList()
        { 
            List<SpecialityModel> list = new List<SpecialityModel>();
            using (SqlConnection connection = new SqlConnection(ConfigurationModel.getConnectionString("toUniver")))
            {
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.Parameters.Clear(); 
                    command.CommandText = @" select
                                                sp.speciality_okpd as code,
                                                sp.speciality_name_kz as nameKz,
                                                sp.speciality_name_ru as nameRu,
                                                sp.speciality_name_en as nameEn,
                                                0 as totalCredit,
                                                sp.speciality_id  as LmsSpecId, 
                                                sp.speciality_qualific_kz as QualificationKZ,
                                                sp.speciality_qualific_ru as QualificationRU,
                                                sp.speciality_qualific_en as QualificationEN,
                                                sp.type,
                                                sp.stage_id,
                                                (case when sp.status=1 then 0 else 1 end) as IsDeleted
                                            from univer_speciality sp 
                                            where 
                                                exists(select 1 from univer_students s where sp.speciality_id=s.speciality_id)
                                                or exists(select 1 from _arc_univer_students s where sp.speciality_id=s.speciality_id)";

                    connection.Open();
                    using (SqlDataReader read = command.ExecuteReader())
                    {
                        if (read.HasRows)
                        {
                            while (read.Read())
                            {
                                SpecialityModel sp = new SpecialityModel();
                                sp.Code = read["Code"] as string;
                                sp.LmsSpecId = (int)read["LmsSpecId"];
                                sp.TotalCredit = (int)read["totalCredit"];
                                sp.NameKZ = read["nameKz"] as string;
                                sp.NameRU = read["NameRU"] as string;
                                sp.NameEN = read["NameEN"] as string;
                                sp.QualificationEN = read["QualificationEN"] as string;
                                sp.QualificationKZ = read["QualificationKZ"] as string;
                                sp.QualificationRU = read["QualificationRU"] as string;
                                sp.Type = (int)read["type"];
                                sp.StageId = (int)read["stage_id"];
                                sp.IsDeleted = Convert.ToBoolean(read["IsDeleted"]);
                                list.Add(sp);
                            }
                        }
                    }
                }
            }
            return list;
        }
    }
}
